from ROOT import TFile, TTree, gROOT, TMath, TLorentzVector, vector, TRandom3
from array import array

#gROOT.ProcessLine(".L TopDileptonReconstruction_cxx.so")
gROOT.ProcessLine(".L NeutrinoWeighting_cxx.so")

#from ROOT import TopDileptonReconstruction
from ROOT import NeutrinoWeighting

FIXED   = 1
DYNAMIC = 2

def copy_tree(input_file, dsid, output_file, branches_to_turn_on, resolution_setting, resolution_number):


    ### Open the input ROOT file
    input_root_file = TFile(input_file, "READ")


    ### Get the input TTree
    input_tree = input_root_file.Get("AnalysisMiniTree")

    ### Determine period
    period_string = ""
    hist_number   = ""
    if "r13167" in input_file or "mc20a" in input_file:
        period_string = "mc20a"
        hist_number   = "284500"
    elif "r13144" in input_file or "mc20d" in input_file:
        period_string = "mc20d"
        hist_number   = "300000"
    elif "r13145" in input_file or "mc20e" in input_file:
        period_string = "mc20e"
        hist_number   = "310000"
    elif "data" in input_file:
        period_string = "data"
    else:
        print("ERROR: Didn't understand period",input_file)

    ### Get the scaling histogram
    hist_name = "CutBookkeeper_" + str(dsid) + "_" + hist_number + "_NOSYS"
    nevents_histogram = input_root_file.Get(hist_name)
    print(hist_name)
    gROOT.cd()

    ### Create a new output ROOT file
    output_root_file = TFile(output_file + "_" + period_string + ".root", "RECREATE")
    if not period_string == "data":
        nevents_histogram = nevents_histogram.Clone()
        nevents_histogram.Write()

    ### Activate only the branches you want to copy 
    input_tree.SetBranchStatus("*",0)
    for branch_name in branches_to_turn_on:
        input_tree.SetBranchStatus(branch_name, 1)


    ### Create a new output TTree and new branches
    output_tree = input_tree.CloneTree(0)


    #These are all vectors because it's easier to do it that way in pyROOT
    NW_weight     = vector('float')()
    NW_solutions  = vector('float')()
    lep_pt        = vector('float')()
    lep_eta       = vector('float')()
    lep_phi       = vector('float')()
    lep_e         = vector('float')()
    lep_pid       = vector('float')()
    bjet_pt       = vector('float')()
    bjet_eta      = vector('float')()
    bjet_phi      = vector('float')()
    bjet_e        = vector('float')()
    bjet_n        = vector('float')()
    weight_mc     = vector('float')()
    weight_btag   = vector('float')()
    weight_pileup = vector('float')()
    weight_jvt    = vector('float')()
    mbb           = vector('float')()
    mll           = vector('float')()
    mlb           = vector('float')()
    mbbll         = vector('float')()
    dRbb          = vector('float')()
    dRll          = vector('float')()
    dRbbll        = vector('float')()
    pTbb          = vector('float')()
    pTll          = vector('float')()
    pTbbll        = vector('float')()
    dPhibb        = vector('float')()
    dPhill        = vector('float')()
    dPhibbll      = vector('float')()
    flag_channel  = vector('float')() #0 = ee, 1 = mm, 2 = em

    vectors = []
    vectors.append(NW_weight    )
    vectors.append(NW_solutions )
    vectors.append(lep_pt       )
    vectors.append(lep_eta      )
    vectors.append(lep_phi      )
    vectors.append(lep_e        )
    vectors.append(lep_pid      )
    vectors.append(bjet_pt      )
    vectors.append(bjet_eta     )
    vectors.append(bjet_phi     )
    vectors.append(bjet_e       )
    vectors.append(bjet_n       )
    vectors.append(weight_mc    )
    vectors.append(weight_btag  )
    vectors.append(weight_pileup)
    vectors.append(weight_jvt   )
    vectors.append(mbb          )
    vectors.append(mll          )
    vectors.append(mlb          )    
    vectors.append(mbbll        )
    vectors.append(dRbb         )
    vectors.append(dRll         )
    vectors.append(dRbbll       )
    vectors.append(pTbb         )
    vectors.append(pTll         )
    vectors.append(pTbbll       )
    vectors.append(dPhibb       )
    vectors.append(dPhill       )
    vectors.append(dPhibbll     )
    vectors.append(flag_channel )

    output_tree.Branch("NW_weight",    NW_weight)
    output_tree.Branch("NW_solutions", NW_solutions)
    output_tree.Branch("lep_pt",       lep_pt)
    output_tree.Branch("lep_eta",      lep_eta)
    output_tree.Branch("lep_phi",      lep_phi)
    output_tree.Branch("lep_e",        lep_e)
    output_tree.Branch("lep_pid",      lep_pid)
    output_tree.Branch("bjet_pt",      bjet_pt)
    output_tree.Branch("bjet_eta",     bjet_eta)
    output_tree.Branch("bjet_phi",     bjet_phi)
    output_tree.Branch("bjet_e",       bjet_e)
    output_tree.Branch("bjet_n",       bjet_n)
    output_tree.Branch("weight_mc",    weight_mc)
    output_tree.Branch("weight_btag",  weight_btag)
    output_tree.Branch("weight_pileup",weight_pileup)
    output_tree.Branch("weight_jvt",   weight_jvt)
    output_tree.Branch("mbb",          mbb)
    output_tree.Branch("mll",          mll)
    output_tree.Branch("mlb",          mlb)    
    output_tree.Branch("mbbll",        mbbll)    
    output_tree.Branch("dRbb",         dRbb)
    output_tree.Branch("dRll",         dRll)   
    output_tree.Branch("dRbbll",       dRbbll)       
    output_tree.Branch("pTbb",         pTbb)
    output_tree.Branch("pTll",         pTll) 
    output_tree.Branch("pTbbll",       pTbbll)     
    output_tree.Branch("dPhibb",       dPhibb) 
    output_tree.Branch("dPhill",       dPhill)
    output_tree.Branch("dPhibbll",     dPhibbll)
    output_tree.Branch("flag_channel", flag_channel)

    ### Loop over the entries in the input TTree

    total = input_tree.GetEntries()
    max_events = -1

    ### Count the number of selected events    
    passed_events = 0


    ### run NuW (with both possible lb pairings)
    reco_pairing_1 = NeutrinoWeighting()
    reco_pairing_2 = NeutrinoWeighting()

    reco_pairing_1.SetEtaSamplingSMLep(True)
    reco_pairing_2.SetEtaSamplingSMLep(True)   
    reco_pairing_1.SetEtaSamplingDoRandom(True)    
    reco_pairing_2.SetEtaSamplingDoRandom(True)  
    reco_pairing_1.SetRandomSeed(12345)
    reco_pairing_2.SetRandomSeed(12345)
    #reco_pairing_1.StopAfterFirstSolution(False)
    #reco_pairing_2.StopAfterFirstSolution(False)
    reco_pairing_1.StopAfterFirstSolution(False)
    reco_pairing_2.StopAfterFirstSolution(False)

    reco_pairing_1.SetEtaSamplingNsamples(20)
    reco_pairing_2.SetEtaSamplingNsamples(20)

    if resolution_setting == FIXED:
        reco_pairing_1.SetFlagResFixed(True)
        reco_pairing_2.SetFlagResFixed(True)
        reco_pairing_1.SetResFixedValue(resolution_number)
        reco_pairing_2.SetResFixedValue(resolution_number)          
    elif resolution_setting == DYNAMIC:
        reco_pairing_1.SetFlagResDynamicConstXMet(True)
        reco_pairing_2.SetFlagResDynamicConstXMet(True)
        reco_pairing_1.SetResConstXMetValue(resolution_number)
        reco_pairing_2.SetResConstXMetValue(resolution_number)     
    else:
        print("Didn't Understand resoltuion setting!")

    for entry in range(input_tree.GetEntries()):

        if entry % 1000 == 0:
            print(entry,"/",total)

        if entry > max_events and not max_events == -1:
            break
        input_tree.GetEntry(entry)

        for vec in vectors:
            vec.clear()

        ### Reset new branches
        #initialise these to -2
        NW_weight.push_back(-2)
        NW_solutions.push_back(-2)        

        ### Do preselection
        if not input_tree.bbll_TWO_OPPOSITE_CHARGE_LEPTONS_NOSYS:
            continue
        if not input_tree.bbll_EXACTLY_TWO_B_JETS_NOSYS:
            continue
        ### Select EMu only ###
        #if not input_tree.bbll_IS_em_NOSYS:
        #    continue

        if abs(input_tree.bbll_Lepton1_pdgid*input_tree.bbll_Lepton2_pdgid) == 143:
            flag_channel.push_back(2)
        elif abs(input_tree.bbll_Lepton1_pdgid*input_tree.bbll_Lepton2_pdgid) == 121:
            flag_channel.push_back(0)
        elif abs(input_tree.bbll_Lepton1_pdgid*input_tree.bbll_Lepton2_pdgid) == 169:
            flag_channel.push_back(1)
        else:
            print("couldn't resolve lepton IDs",input_tree.bbll_Lepton1_pdgid,input_tree.bbll_Lepton2_pdgid)
            continue

        passed_events += 1

        reco_pairing_1.Reset()
        reco_pairing_2.Reset()        

        weight_mc.push_back(input_tree.mcEventWeights[0])
        weight_btag.push_back(1)
        weight_jvt.push_back(1)
        weight_pileup.push_back(1) 

        lep_pt.push_back(input_tree.bbll_Lepton1_pt_NOSYS/1000.)
        lep_pt.push_back(input_tree.bbll_Lepton2_pt_NOSYS/1000.)
        lep_eta.push_back(input_tree.bbll_Lepton1_eta)
        lep_eta.push_back(input_tree.bbll_Lepton2_eta)        
        lep_phi.push_back(input_tree.bbll_Lepton1_phi)
        lep_phi.push_back(input_tree.bbll_Lepton2_phi) 
        lep_e.push_back(input_tree.bbll_Lepton1_E/1000.)
        lep_e.push_back(input_tree.bbll_Lepton2_E/1000.) 

        bjet_pt.push_back(input_tree.bbll_Jet_b1_pt_NOSYS/1000.)
        bjet_pt.push_back(input_tree.bbll_Jet_b2_pt_NOSYS/1000.)
        bjet_eta.push_back(input_tree.bbll_Jet_b1_eta)
        bjet_eta.push_back(input_tree.bbll_Jet_b2_eta)        
        bjet_phi.push_back(input_tree.bbll_Jet_b1_phi)
        bjet_phi.push_back(input_tree.bbll_Jet_b2_phi) 
        bjet_e.push_back(input_tree.bbll_Jet_b1_E/1000.)
        bjet_e.push_back(input_tree.bbll_Jet_b2_E/1000.) 

        ### Setup input 4vectors
        lep_p = TLorentzVector()
        lep_m = TLorentzVector()
        b1    = TLorentzVector()
        b2    = TLorentzVector()
        top   = TLorentzVector()
        tbar  = TLorentzVector()
        ttbar = TLorentzVector()
        nu    = TLorentzVector()
        nubar = TLorentzVector()

        if (input_tree.bbll_Lepton1_charge > 0):
            lep_p.SetPtEtaPhiE(input_tree.bbll_Lepton1_pt_NOSYS/1000., input_tree.bbll_Lepton1_eta, input_tree.bbll_Lepton1_phi, input_tree.bbll_Lepton1_E/1000.)
            lep_m.SetPtEtaPhiE(input_tree.bbll_Lepton2_pt_NOSYS/1000., input_tree.bbll_Lepton2_eta, input_tree.bbll_Lepton2_phi, input_tree.bbll_Lepton2_E/1000.)
        else:
            lep_p.SetPtEtaPhiE(input_tree.bbll_Lepton2_pt_NOSYS/1000., input_tree.bbll_Lepton2_eta, input_tree.bbll_Lepton2_phi, input_tree.bbll_Lepton2_E/1000.)
            lep_m.SetPtEtaPhiE(input_tree.bbll_Lepton1_pt_NOSYS/1000., input_tree.bbll_Lepton1_eta, input_tree.bbll_Lepton1_phi, input_tree.bbll_Lepton1_E/1000.)

        b1.SetPtEtaPhiE(input_tree.bbll_Jet_b1_pt_NOSYS/1000., input_tree.bbll_Jet_b1_eta, input_tree.bbll_Jet_b1_phi, input_tree.bbll_Jet_b1_E/1000.)
        b2.SetPtEtaPhiE(input_tree.bbll_Jet_b2_pt_NOSYS/1000., input_tree.bbll_Jet_b2_eta, input_tree.bbll_Jet_b2_phi, input_tree.bbll_Jet_b2_E/1000.)

        met_ex = (input_tree.met_NOSYS_met/1000.)*TMath.Cos(input_tree.met_NOSYS_phi)
        met_ey = (input_tree.met_NOSYS_met/1000.)*TMath.Sin(input_tree.met_NOSYS_phi)

        bb = b1 + b2
        ll = lep_p + lep_m
        bbll = bb + ll

        #pair the mlbs
        b1l = None
        b2l = None
        if b1.DeltaR(lep_p) < b1.DeltaR(lep_m):
            b1l = b1 + lep_p
            b2l = b2 + lep_m
        else:
            b1l = b1 + lep_m
            b2l = b2 + lep_p

        mlb.push_back(b1l.M())
        mlb.push_back(b2l.M())

        mbb.push_back(bb.M())
        dRbb.push_back(b1.DeltaR(b2))
        pTbb.push_back(bb.Pt())
        dPhibb.push_back(b1.DeltaPhi(b2))

        mll.push_back(ll.M())
        dRll.push_back(lep_p.DeltaR(lep_m))
        pTll.push_back(ll.Pt())
        dPhill.push_back(lep_p.DeltaPhi(lep_m))

        mbbll.push_back(bbll.M())
        dRbbll.push_back(bb.DeltaR(ll))
        pTbbll.push_back(bbll.Pt())
        dPhibbll.push_back(bb.DeltaPhi(ll))




        # set to -1 here so we know if NW could have actually ran
        NW_weight[0]    = -1.
        NW_solutions[0] = -1.

        random = TRandom3(1000)

        DO_NW = False
        if DO_NW:
            for i in range(0,1):#10
    
                mtop  = random.Gaus(172.5, 1.480)
                mtbar = random.Gaus(172.5, 1.480)            
                mWpos = random.Gaus(80.379, 2.085)
                mWneg = random.Gaus(80.379, 2.085)            
    
                reco_pairing_1.Reconstruct(lep_p,  lep_m,  b1,  b2, met_ex, met_ey, mtop, mtbar, mWpos, mWneg);
                reco_pairing_2.Reconstruct(lep_p,  lep_m,  b2,  b1, met_ex, met_ey, mtop, mtbar, mWpos, mWneg);
    
    
                # keep the one with the highest weight
                if reco_pairing_1.GetWeight() > 0 or reco_pairing_2.GetWeight() > 0:
                    if (reco_pairing_1.GetWeight() > reco_pairing_2.GetWeight()):
                        NW_weight[0]    = reco_pairing_1.GetWeight()
                        NW_solutions[0] = (reco_pairing_1.GetWeights()).size()
                    else:
                        NW_weight[0]    = reco_pairing_2.GetWeight()
                        NW_solutions[0] = (reco_pairing_2.GetWeights()).size()


        ### Write everything to output tree
        output_tree.Fill()


    ### Write the output TTree to the output file
    output_tree.Write()

    print("Sample has ",passed_events, "passed events")


    ### Close the input and output ROOT files
    input_root_file.Close()
    output_root_file.Close()


if __name__ == "__main__":

    path_to_unskimmed_files = "./NTuples_p6026/"

    paths_to_run = [#["./2L_OS_2B_10Scans_Gauss_SMLepAssumption_NoSmearing_res1timesMet/",            DYNAMIC,  1], 
                    #["./2L_OS_2B_10Scans_Gauss_SMLepAssumption_NoSmearing_res2timesMet/",            DYNAMIC,  2],
                    #["./2L_OS_2B_10Scans_Gauss_SMLepAssumption_NoSmearing_res4timesMet/",            DYNAMIC,  4],
                    #["./2L_OS_2B_10Scans_Gauss_SMLepAssumption_NoSmearing_res10/",                   FIXED,   10],
                    #["./2L_OS_2B_10Scans_Gauss_SMLepAssumption_NoSmearing_res20/",                   FIXED,   20],                    
                    #["./2L_OS_2B_10Scans_Gauss_SMLepAssumption_NoSmearing_res40/",                   FIXED,   40], 
                    #["./2L_OS_2B_10Scans_Gauss_SMLepAssumption_NoSmearing_res2timesMet_StopAtFirst/", DYNAMIC,  2],
                    ["./2L_OS_2B_NoNW/", -1, -1],                                       
                    ]






    #input_files_mc16a = [#[path_to_unskimmed_files + "DiHiggs/user.gcallea.Test130324v2.600465.PhPy8EG_PDF4LHC15_HHbbWW2l_cHHH01d0.e8222_s3681_r13167_p5855.root",       600465, "DiHiggs_bbWW_skimmed"],
    #                     #[path_to_unskimmed_files + "DiHiggs/user.gcallea.Test130324v2.600469.PhPy8EG_PDF4LHC15_HHbbZZ2l_cHHH01d0.e8222_s3681_r13167_p5855.root",       600469, "DiHiggs_bbZZ_skimmed"],
    #                     #[path_to_unskimmed_files + "DiHiggs/user.gcallea.Test130324v2.600467.PhPy8EG_PDF4LHC15_HHbbtt2l_cHHH01d0.e8222_s3681_r13167_p5855.root",       600467, "DiHiggs_bbtautau_skimmed"],
    #                     #[path_to_unskimmed_files + "Ztautau/user.gcallea.Test120324v2.700326.Sh_2211_Ztautau_LL_maxHTpTV2_BFilter.e8351_s3681_r13167_p5855.root",      700326, "Ztautau_BFilter_skimmed"],
    #                     #[path_to_unskimmed_files + "Ztautau/user.gcallea.Test120324v2.700327.Sh_2211_Ztautau_LL_maxHTpTV2_CFilterBVeto.e8351_s3681_r13167_p5855.root", 700327, "Ztautau_BVetoCFilter_skimmed"],                   
    #                     ###[path_to_unskimmed_files + "Ztautau/user.gcallea.Test120324v2.700328.Sh_2211_Ztautau_LL_maxHTpTV2_CVetoBVeto.e8351_s3681_r13167_p5855.root",   700328, "Ztautau_BVetoCVeto_skimmed"],
    #                    #[path_to_unskimmed_files + "Zmumu/user.gcallea.Test120324v2.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.e8351_s3681_r13167_p5855.root",             700323, "Zmumu_BFilter_skimmed"],
    #                    #[path_to_unskimmed_files + "Zmumu/user.gcallea.Test120324v2.700324.Sh_2211_Zmumu_maxHTpTV2_CFilterBVeto.e8351_s3681_r13167_p5855.root",        700324, "Zmumu_BVetoCFilter_skimmed"],
    #                    #[path_to_unskimmed_files + "Zmumu/user.gcallea.Test120324v2.700325.Sh_2211_Zmumu_maxHTpTV2_CVetoBVeto.e8351_s3681_r13167_p5855.root",          700325, "Zmumu_BVetoCVeto_skimmed"],             
    #                    #[path_to_unskimmed_files + "Zee/user.gcallea.Test120324v2.700320.Sh_2211_Zee_maxHTpTV2_BFilter.e8351_s3681_r13167_p5855.root",                 700320, "Zee_BFilter_skimmed"],
    #                    #[path_to_unskimmed_files + "Zee/user.gcallea.Test120324v2.700321.Sh_2211_Zee_maxHTpTV2_CFilterBVeto.e8351_s3681_r13167_p5855.root",            700321, "Zee_BVetoCFilter_skimmed"],
    #                    #[path_to_unskimmed_files + "Zee/user.gcallea.Test120324v2.700322.Sh_2211_Zee_maxHTpTV2_CVetoBVeto.e8351_s3681_r13167_p5855.root",              700322, "Zee_BVetoCVeto_skimmed"],            
    #                     [path_to_unskimmed_files + "Wt/user.gcallea.Test120324v2.410648.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_top.e6615_s3681_r13167_p5855.root",     410648, "Wt_dilep_skimmed"],
    #                     [path_to_unskimmed_files + "Wt/user.gcallea.Test120324v2.410649.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_antitop.e6615_s3681_r13167_p5855.root", 410649, "Wtbar_dilep_skimmed"],
    #                     [path_to_unskimmed_files + "Data/data15_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_PHYSLITE.grp15_v01_p5855.root",                         999999, "Data_15_skimmed"],                           
    #                     [path_to_unskimmed_files + "Data/data16_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_PHYSLITE.grp16_v01_p5855.root",                         999999, "Data_16_skimmed"],                                                    
    #                     [path_to_unskimmed_files + "ttbar/user.gcallea.Test120324v2.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.e6348_s3681_r13167_p5855.root",           410472, "ttbar_dilep_skimmed"],
    #                              
    #]
#
    #input_files_mc16e = [[path_to_unskimmed_files + "DiHiggs/user.gcallea.Test130324v2.600465.PhPy8EG_PDF4LHC15_HHbbWW2l_cHHH01d0.e8222_s3681_r13145_p5855.root",       600465, "DiHiggs_bbWW_skimmed"],
    #                     [path_to_unskimmed_files + "DiHiggs/user.gcallea.Test130324v2.600469.PhPy8EG_PDF4LHC15_HHbbZZ2l_cHHH01d0.e8222_s3681_r13145_p5855.root",       600469, "DiHiggs_bbZZ_skimmed"],
    #                     [path_to_unskimmed_files + "DiHiggs/user.gcallea.Test130324v2.600467.PhPy8EG_PDF4LHC15_HHbbtt2l_cHHH01d0.e8222_s3681_r13145_p5855.root",       600467, "DiHiggs_bbtautau_skimmed"],
    #                     [path_to_unskimmed_files + "Ztautau/user.gcallea.Test120324v2.700326.Sh_2211_Ztautau_LL_maxHTpTV2_BFilter.e8351_s3681_r13145_p5855.root",      700326, "Ztautau_BFilter_skimmed"],
    #                     [path_to_unskimmed_files + "Ztautau/user.gcallea.Test120324v2.700327.Sh_2211_Ztautau_LL_maxHTpTV2_CFilterBVeto.e8351_s3681_r13145_p5855.root", 700327, "Ztautau_BVetoCFilter_skimmed"],                   
    #                     ###[path_to_unskimmed_files + "Ztautau/user.gcallea.Test120324v2.700328.Sh_2211_Ztautau_LL_maxHTpTV2_CVetoBVeto.e8351_s3681_r13167_p5855.root",   700328, "Ztautau_BVetoCVeto_skimmed"],
    #                    #[path_to_unskimmed_files + "Zmumu/user.gcallea.Test120324v2.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.e8351_s3681_r13167_p5855.root",             700323, "Zmumu_BFilter_skimmed"],
    #                    #[path_to_unskimmed_files + "Zmumu/user.gcallea.Test120324v2.700324.Sh_2211_Zmumu_maxHTpTV2_CFilterBVeto.e8351_s3681_r13167_p5855.root",        700324, "Zmumu_BVetoCFilter_skimmed"],
    #                    #[path_to_unskimmed_files + "Zmumu/user.gcallea.Test120324v2.700325.Sh_2211_Zmumu_maxHTpTV2_CVetoBVeto.e8351_s3681_r13167_p5855.root",          700325, "Zmumu_BVetoCVeto_skimmed"],             
    #                    #[path_to_unskimmed_files + "Zee/user.gcallea.Test120324v2.700320.Sh_2211_Zee_maxHTpTV2_BFilter.e8351_s3681_r13167_p5855.root",                 700320, "Zee_BFilter_skimmed"],
    #                    #[path_to_unskimmed_files + "Zee/user.gcallea.Test120324v2.700321.Sh_2211_Zee_maxHTpTV2_CFilterBVeto.e8351_s3681_r13167_p5855.root",            700321, "Zee_BVetoCFilter_skimmed"],
    #                    #[path_to_unskimmed_files + "Zee/user.gcallea.Test120324v2.700322.Sh_2211_Zee_maxHTpTV2_CVetoBVeto.e8351_s3681_r13167_p5855.root",              700322, "Zee_BVetoCVeto_skimmed"],            
    #                     [path_to_unskimmed_files + "Wt/user.gcallea.Test120324v2.410648.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_top.e6615_s3681_r13145_p5855.root",     410648, "Wt_dilep_skimmed"],
    #                     [path_to_unskimmed_files + "Wt/user.gcallea.Test120324v2.410649.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_antitop.e6615_s3681_r13145_p5855.root", 410649, "Wtbar_dilep_skimmed"],
    #                     [path_to_unskimmed_files + "Data/data18_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_PHYSLITE.grp18_v01_p5855.root",                         999999, "Data_18_skimmed"],                                                    
    #                     [path_to_unskimmed_files + "ttbar/user.gcallea.Test120324v2.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.e6348_s3681_r13145_p5855.root",           410472, "ttbar_dilep_skimmed"],
    #                              
    #]

    input_files = [#[path_to_unskimmed_files + "DiHiggs/",                                600465, "DiHiggs_bbWW_skimmed"],
                   #[path_to_unskimmed_files + "DiHiggs/",                                600469, "DiHiggs_bbZZ_skimmed"],
                   #[path_to_unskimmed_files + "DiHiggs/",                                600467, "DiHiggs_bbtautau_skimmed"],
                   ###[path_to_unskimmed_files + "Ztautau/Ztautau_700792_mc20a_p6026.root", 700792, "Ztautau_BFilter_skimmed"],
                   ###[path_to_unskimmed_files + "Ztautau/Ztautau_700792_mc20d_p6026.root", 700792, "Ztautau_BFilter_skimmed"],
                   ###[path_to_unskimmed_files + "Ztautau/Ztautau_700792_mc20e_p6026.root", 700792, "Ztautau_BFilter_skimmed"],                                                  
                   #[path_to_unskimmed_files + "Ztautau/",                                700, "Ztautau_BVetoCFilter_skimmed"],                   
                   #[path_to_unskimmed_files + "Ztautau/",                                700, "Ztautau_BVetoCFilter_skimmed"],                   
                   #[path_to_unskimmed_files + "Ztautau/",                                700, "Ztautau_BVetoCFilter_skimmed"],                                                                     
                   #[path_to_unskimmed_files + "Ztautau/",                                700, "Ztautau_BVetoCVeto_skimmed"],
                   #[path_to_unskimmed_files + "Ztautau/",                                700, "Ztautau_BVetoCVeto_skimmed"],
                   #[path_to_unskimmed_files + "Ztautau/",                                700, "Ztautau_BVetoCVeto_skimmed"] ,                                                 
                   ###[path_to_unskimmed_files + "Zmumu/Zmumu_700323_mc20a_p6026.root",     700323, "Zmumu_BFilter_skimmed"],
                   ###[path_to_unskimmed_files + "Zmumu/Zmumu_700323_mc20d_p6026.root",     700323, "Zmumu_BFilter_skimmed"],
                   ###[path_to_unskimmed_files + "Zmumu/Zmumu_700323_mc20e_p6026.root",     700323, "Zmumu_BFilter_skimmed"],                                                  
                   #[path_to_unskimmed_files + "Zmumu/",                                  700324, "Zmumu_BVetoCFilter_skimmed"],
                   #[path_to_unskimmed_files + "Zmumu/",                                  700324, "Zmumu_BVetoCFilter_skimmed"],
                   #[path_to_unskimmed_files + "Zmumu/",                                  700324, "Zmumu_BVetoCFilter_skimmed"],                                                  
                   #[path_to_unskimmed_files + "Zmumu/",                                  700325, "Zmumu_BVetoCVeto_skimmed"],             
                   #[path_to_unskimmed_files + "Zmumu/",                                  700325, "Zmumu_BVetoCVeto_skimmed"],             
                   #[path_to_unskimmed_files + "Zmumu/",                                  700325, "Zmumu_BVetoCVeto_skimmed"],                                                               
                   ###[path_to_unskimmed_files + "Zee/Zee_700320_mc20a.root",               700320, "Zee_BFilter_skimmed"],
                ##[path_to_unskimmed_files + "Zee/Zee_700320_mc20d.root",               700320, "Zee_BFilter_skimmed"],
                ##[path_to_unskimmed_files + "Zee/Zee_700320_mc20e.root",               700320, "Zee_BFilter_skimmed"],                                                  
                   #[path_to_unskimmed_files + "Zee/",                                    700321, "Zee_BVetoCFilter_skimmed"],
                   #[path_to_unskimmed_files + "Zee/",                                    700321, "Zee_BVetoCFilter_skimmed"],
                   #[path_to_unskimmed_files + "Zee/",                                    700321, "Zee_BVetoCFilter_skimmed"],                                                  
                   #[path_to_unskimmed_files + "Zee/",                                    700322, "Zee_BVetoCVeto_skimmed"],            
                   #[path_to_unskimmed_files + "Zee/",                                    700322, "Zee_BVetoCVeto_skimmed"],            
                   #[path_to_unskimmed_files + "Zee/",                                    700322, "Zee_BVetoCVeto_skimmed"],                                                              
                   #[path_to_unskimmed_files + "Wt/",                                     410648, "Wt_dilep_skimmed"],
                   #[path_to_unskimmed_files + "Wt/",                                     410648, "Wt_dilep_skimmed"],
                   #[path_to_unskimmed_files + "Wt/",                                     410648, "Wt_dilep_skimmed"],                                                  
                   #[path_to_unskimmed_files + "Wt/",                                     410649, "Wtbar_dilep_skimmed"],
                   #[path_to_unskimmed_files + "Wt/",                                     410649, "Wtbar_dilep_skimmed"],
                   #[path_to_unskimmed_files + "Wt/",                                     410649, "Wtbar_dilep_skimmed"],                                                  
                   ###[path_to_unskimmed_files + "Data/data15_p6026.root",                  999999, "Data_15_skimmed"],
                   ###[path_to_unskimmed_files + "Data/data16_p6026.root",                  999999, "Data_16_skimmed"],   
                   ###[path_to_unskimmed_files + "Data/data17_p6026.root",                  999999, "Data_17_skimmed"],
                   ###[path_to_unskimmed_files + "Data/data18_p6026.root",                  999999, "Data_18_skimmed"],
                   ###[path_to_unskimmed_files + "ttbar/ttbar_410472_mc20a_p6026.root",     410472, "ttbar_dilep_skimmed"],
                   ###[path_to_unskimmed_files + "ttbar/ttbar_410472_mc20d_p6026.root",     410472, "ttbar_dilep_skimmed"],
                   ###[path_to_unskimmed_files + "ttbar/ttbar_410472_mc20e_p6026.root",     410472, "ttbar_dilep_skimmed"],                                                                             
    ] 


#    input_files = [[path_to_unskimmed_files + "Ztautau/Ztautau_700792_mc20a_p6026.root", 700792, "Ztautau_BFilter_skimmed"],
#    ] 


    # List of branches to copy
    branches_to_copy = ["mcEventWeights",
                        "bbll_Jet_*",
                        #"bbll_Jet1_pt_NOSYS",
                        #"bbll_Jet2_pt_NOSYS",
                        #"bbll_Jet1_eta",
                        #"bbll_Jet2_eta",
                        #"bbll_Jet1_phi",
                        #"bbll_Jet2_phi",
                        #"bbll_Jet1_E",
                        #"bbll_Jet2_E",
                        #"bbll_Jet_b1_pt_NOSYS",
                        #"bbll_Jet_b2_pt_NOSYS",
                        #"bbll_Jet_b1_eta",
                        #"bbll_Jet_b2_eta",
                        #"bbll_Jet_b1_phi",
                        #"bbll_Jet_b2_phi",
                        #"bbll_Jet_b1_E",
                        #"bbll_Jet_b2_E",
                        "bbll_Lepton*",
                        #"bbll_Lepton1_pt_NOSYS",
                        #"bbll_Lepton2_pt_NOSYS",
                        #"bbll_Lepton1_eta",
                        #"bbll_Lepton2_eta",
                        #"bbll_Lepton1_phi",
                        #"bbll_Lepton2_phi",
                        #"bbll_Lepton1_E",
                        #"bbll_Lepton2_E",
                        #"bbll_Lepton1_charge",
                        #"bbll_Lepton2_charge",
                        #"bbll_Lepton1_pdgid",
                        #"bbll_Lepton2_pdgid",
                        "weight_*",
                        "PileupWeight_NOSYS",
                        "generatorWeight_NOSYS",
                        #"bbll_nBJets_NOSYS",
                        #"bbll_nElectrons_NOSYS",
                        #"bbll_nMuons_NOSYS",
                        #"bbll_nJets_NOSYS",
                        #"bbll_dRbb",
                        #"bbll_Etabb",
                        #"bbll_Phibb",
                        #"bbll_mbb_NOSYS",
                        #"bbll_pTbb_NOSYS",
                        #"bbll_pass_SR_NOSYS",
                        "bbll_EXACTLY_TWO_LEPTONS_NOSYS",
                        "bbll_EXACTLY_TWO_B_JETS_NOSYS",
                        #"bbll_pass_trigger_DLT_NOSYS"   
                        #"bbll_pass_trigger_ASLT1_me_NOSYS" 
                        #"bbll_pass_trigger_ASLT2_NOSYS",                    
                        #"bbll_pass_trigger_SLT_NOSYS",
                        #"bbll_pass_SR_NOSYS",
                        #"bbll_Pass_ll_NOSYS",
                        #"bbll_PASS_TRIGGER_NOSYS",
                        "bbll_TWO_OPPOSITE_CHARGE_LEPTONS_NOSYS",
                        #"bbll_DILEPTON_MASS_SR1_NOSYS",
                        #"bbll_VBFVETO_SR1_NOSYS",
                        #"bbll_DILEPTON_MASS_SR2_NOSYS",
                        #"bbll_DIBJET_MASS_SR2_NOSYS",
                        "bbll_IS_em_NOSYS",
                        "bbll_IS_ee_NOSYS",
                        "bbll_IS_SF_NOSYS",
                        "met_NOSYS_met",
                        "met_NOSYS_phi",
                        #"met_NOSYS_sumet",
                        #"truth_H1_pdgId",
                        #"truth_H2_pdgId",
                        #"truth_children_fromH1_pdgId",
                        #"truth_children_fromH2_pdgId",
                        #"truth_H1_pt",
                        #"truth_H1_eta",
                        #"truth_H1_phi",
                        #"truth_H1_m",
                        #"truth_H2_pt",
                        #"truth_H2_eta",
                        #"truth_H2_phi",
                        #"truth_H2_m",
                        #"truth_HH_pt",
                        #"truth_HH_eta",
                        #"truth_HH_phi",
                        #"truth_HH_m",
                        ]


    ### Call the function to copy the tree

    for out_path in paths_to_run:

        output_path = out_path[0]

        for ifile in input_files:
            copy_tree(input_file          = ifile[0], 
                      dsid                = ifile[1],
                      output_file         = output_path + ifile[2], 
                      branches_to_turn_on = branches_to_copy,
                      resolution_setting  = out_path[1],
                      resolution_number   = out_path[2])




