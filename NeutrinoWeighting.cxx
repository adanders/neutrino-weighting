#include "./NeutrinoWeighting.h"



NeutrinoWeighting::~NeutrinoWeighting(){}

NeutrinoWeighting::NeutrinoWeighting(){
  Reset();
}

void NeutrinoWeighting::Reset(){

  m_tops.clear();
  m_tbars.clear();
  m_nus.clear();
  m_nubars.clear();
  m_Wposs.clear();
  m_Wnegs.clear();

  m_weights.clear();
  highestWeight = 0;
  m_weight_threshold = 0; // lower limit allowed for weights


  m_highestWeightTop   = TLorentzVector();
  m_highestWeightTbar  = TLorentzVector();
  m_highestWeightNu    = TLorentzVector();
  m_highestWeightNubar = TLorentzVector();
  m_highestWeightWpos  = TLorentzVector();
  m_highestWeightWneg  = TLorentzVector();

  flag_eta_sampling_linear    = false; // sampling in equal step sizes
  flag_eta_sampling_SM_lep    = true;  // sampling using lep-nu eta SM relationship
  flag_eta_sampling_gauss     = false; // sampling using a user-defined gaussian
  flag_eta_sampling_do_random = true;  // sample using random numbers, not for linear

  eta_sampling_linear_step_low  = -5.;
  eta_sampling_linear_step_high =  5.;
  eta_sampling_gauss_mean       =  0.;
  eta_sampling_gauss_sigma      =  1.;

  eta_sampling_nsamples = 2; // how many sample points to do 

  flag_res_fixed                = false; // Set a fixed resolution value
  flag_res_dynamic_const_x_met  = true; // Set a dynamic resolution value based on a constant x the MET
  res_fixed_value               = 10; // The fixed MET resolution value in (careful with units!)
  res_dynamic_const_x_met_value = 2; // The constant factor to multiply the MET in dynamic resoluton

  m_eta_points_nu.clear();
  m_eta_points_nubar.clear(); 

  m_random = TRandom3(12345);

  stop_after_first_solution = false;

}


void NeutrinoWeighting::SetupEtaSampling(TLorentzVector lepton_pos, TLorentzVector lepton_neg){


  ///-- This function setups up the sampling of eta steps. --///


  // Catch instances of users re-running when they don't need to
  if (flag_eta_sampling_linear && !flag_eta_sampling_do_random && (m_eta_points_nu.size() > 0 || m_eta_points_nubar.size() > 0)){
    std::cout << "NW: It looks like you have asked for non-random linear sampling, but the eta scan point vectors are already filled." << std::endl;
    std::cout << "NW: This setting only needs to be run once, when the class is instantiated, you don't need to run it each time." << std::endl;    
  }

  // Catch if user has asked for random sampling but hasn't set the seed to something new...
  if (flag_eta_sampling_do_random && m_random.GetSeed() == 12345 && flag_eta_sampling_linear){
    std::cout << "NW: You've asked for random sampling and linear spacing but left the random seed to its initial value" << std::endl;
    std::cout << "NW: This will lead to the same random sample points each time. Consider setting the seed to the event number or something similar with SetRandomSeed()" << std::endl;    
  }

  // Catch inconsistent flag settings 
  if ((flag_eta_sampling_linear && flag_eta_sampling_gauss)||
      (flag_eta_sampling_linear && flag_eta_sampling_SM_lep) ||
      (flag_eta_sampling_gauss  && flag_eta_sampling_SM_lep)){
    std::cout << "NW: You've asked for more than one type of sampling. " << std::endl;
    std::cout << "     flag_eta_sampling_linear = " << flag_eta_sampling_linear << std::endl;
    std::cout << "     flag_eta_sampling_gauss  = " << flag_eta_sampling_gauss  << std::endl;    
    std::cout << "     flag_eta_sampling_SM_lep = " << flag_eta_sampling_SM_lep << std::endl;
    std::cout << "NW: By default we'll run linear sampling with default settings and ignore the others" << std::endl;
    flag_eta_sampling_linear = true;
    flag_eta_sampling_gauss  = false;
    flag_eta_sampling_SM_lep = false;
  }

  m_eta_points_nu.clear();
  m_eta_points_nubar.clear();


  /////////////////////////////////////////////////////////////////////////////
  ///--                        Linear Sampling                            --///
  /////////////////////////////////////////////////////////////////////////////

  if (flag_eta_sampling_linear){
    if(flag_eta_sampling_do_random){
      for(uint i = 0; i < eta_sampling_nsamples; ++i){
        double point = m_random.Uniform(eta_sampling_linear_step_low, eta_sampling_linear_step_high);
        m_eta_points_nu.push_back(   point);
        m_eta_points_nubar.push_back(point);        
      }
    } else {
      double step_size = (eta_sampling_linear_step_high - eta_sampling_linear_step_low)/eta_sampling_nsamples;
      for(uint i = 0; i < eta_sampling_nsamples; ++i){
        m_eta_points_nu.push_back(   eta_sampling_linear_step_low + i*step_size);
        m_eta_points_nubar.push_back(eta_sampling_linear_step_low + i*step_size);        
      }      
    }
  } 

  /////////////////////////////////////////////////////////////////////////////
  ///--                        Gaussian Sampling                          --///
  /////////////////////////////////////////////////////////////////////////////
  else if (flag_eta_sampling_gauss){
    if(flag_eta_sampling_do_random){
      for(uint i = 0; i < eta_sampling_nsamples; ++i){
        m_eta_points_nu.push_back(   m_random.Gaus(eta_sampling_gauss_mean, eta_sampling_gauss_sigma));
        m_eta_points_nubar.push_back(m_random.Gaus(eta_sampling_gauss_mean, eta_sampling_gauss_sigma));
      }
    } else {
      std::cout << "NW: THIS OPTION ISN'T IMPLEMENTED YET! DON'T USE!" << std::endl;
    }    

  }


  /////////////////////////////////////////////////////////////////////////////
  ///--                  SM lep-nu eta relation Sampling                  --///
  /////////////////////////////////////////////////////////////////////////////
  else if (flag_eta_sampling_SM_lep){
    if(flag_eta_sampling_do_random){

      /// Check the leptons were actuall set properly ///
      if (lepton_pos==TLorentzVector() || lepton_neg==TLorentzVector()){
       std::cout << "NW: YOU DIDN'T GIVE ME SENSIBLE LEPTONS TO RUN OVER!" << std::endl;       
      }

      double eta_mean_nu    = lepton_pos.Eta()*0.72 - 0.018*lepton_pos.Eta()*lepton_pos.Eta()*lepton_pos.Eta();
      double eta_mean_nubar = lepton_neg.Eta()*0.72 - 0.018*lepton_neg.Eta()*lepton_neg.Eta()*lepton_neg.Eta();
      double width = 1.16;

      for(uint i = 0; i < eta_sampling_nsamples; ++i){
        m_eta_points_nu.push_back(   m_random.Gaus(eta_mean_nu,    width));
        m_eta_points_nubar.push_back(m_random.Gaus(eta_mean_nubar, width));
      }
    } else {
      std::cout << "NW: THIS OPTION ISN'T IMPLEMENTED YET! DON'T USE!" << std::endl;
    } 

  }
}


void NeutrinoWeighting::Reconstruct(TLorentzVector lepton_pos, 
                                    TLorentzVector lepton_neg, 
                                    TLorentzVector b, 
                                    TLorentzVector bbar, 
                                    double met_ex, 
                                    double met_ey, 
                                    double mtop,
                                    double mtbar,
                                    double mWpos,
                                    double mWneg){


  SetupEtaSampling(lepton_pos, lepton_neg);

  assert(m_eta_points_nu.size() == m_eta_points_nubar.size());

  std::vector<TLorentzVector> solution_nu;
  std::vector<TLorentzVector> solution_nubar;

  for(uint i = 0; i < m_eta_points_nu.size(); i++){
    std::vector<TLorentzVector> temp_solution_nu = solveForNeutrinoEta(&lepton_pos, &b, m_eta_points_nu.at(i), mtop, mWpos);
    solution_nu.insert( solution_nu.end(), temp_solution_nu.begin(), temp_solution_nu.end());

    if(stop_after_first_solution && solution_nu.size()>0){
      break;
    }
  }

  for(uint i = 0; i < m_eta_points_nubar.size(); i++){
    std::vector<TLorentzVector> temp_solution_nubar = solveForNeutrinoEta(&lepton_neg, &bbar, m_eta_points_nubar.at(i), mtbar, mWneg);
    solution_nubar.insert( solution_nubar.end(), temp_solution_nubar.begin(), temp_solution_nubar.end());

    if(stop_after_first_solution && solution_nubar.size()>0){
      break;
    }
  }

  if(solution_nu.size()    == 0) return;
  if(solution_nubar.size() == 0) return;

  TLorentzVector top, tbar, ttbar, Wpos, Wneg, nu, nubar;

  for(uint index_nu = 0; index_nu < solution_nu.size(); ++index_nu){
    for(uint index_nubar = 0; index_nubar < solution_nubar.size(); ++index_nubar){

      nu    = solution_nu.at(index_nu);
      nubar = solution_nubar.at(index_nubar);
      Wpos  = lepton_pos + nu;
      Wneg  = lepton_neg + nubar;  
      top   = Wpos + b;
      tbar  = Wneg + bbar;

      m_tops.push_back(top);
      m_tbars.push_back(tbar);
      m_nus.push_back(nu);
      m_nubars.push_back(nubar);
      m_Wposs.push_back(Wpos);
      m_Wnegs.push_back(Wneg);

      double weight = get_weight(nu, nubar, met_ex, met_ey);
      m_weights.push_back(weight);
      if(weight > highestWeight && weight > m_weight_threshold && top.E() > 0 && tbar.E() > 0){      
      	highestWeight        = weight;
      	m_highestWeightTop   = top;
      	m_highestWeightTbar  = tbar;
      	m_highestWeightNu    = nu;
      	m_highestWeightNubar = nubar;
      	m_highestWeightWpos  = Wpos;
      	m_highestWeightWneg  = Wneg;
      }
    }
  }
  return;
}


std::vector<TLorentzVector> NeutrinoWeighting::solveForNeutrinoEta(TLorentzVector* lepton, 
                                                                   TLorentzVector* bJet, 
                                                                   double nu_eta, 
                                                                   double mtop, 
                                                                   double mW) {

  double nu_cosh = cosh(nu_eta);
  double nu_sinh = sinh(nu_eta);

  double Wmass2  = mW*mW;
  double bmass   = 4.5;
  double Elprime = lepton->E() * nu_cosh - lepton->Pz() * nu_sinh;
  double Ebprime = bJet->E()   * nu_cosh - bJet->Pz()   * nu_sinh;

  double A = (lepton->Py() * Ebprime - bJet->Py() * Elprime) / (bJet->Px() * Elprime - lepton->Px() * Ebprime);
  double B = (Elprime * (mtop * mtop - Wmass2 - bmass * bmass - 2. * lepton->Dot(*bJet)) - Ebprime * Wmass2) / (2. * (lepton->Px() * Ebprime - bJet->Px() * Elprime));

  double par1 = (lepton->Px() * A + lepton->Py()) / Elprime;
  double C    = A * A + 1. - par1 * par1;
  double par2 = (Wmass2 / 2. + lepton->Px() * B) / Elprime;
  double D    = 2. * (A * B - par2 * par1);
  double F    = B * B - par2 * par2;
  double det  = D * D - 4. * C * F;

  std::vector<TLorentzVector> sol;

  ///-- 0 solutions case --///
  if (det < 0.0){
    return std::move(sol);
  }

  ///-- Only one real solution case --///
  if (det == 0.) {
    double py1 = -D / (2. * C);
    double px1 = A * py1 + B;
    double pT2_1 = px1 * px1 + py1 * py1;
    double pz1 = sqrt(pT2_1) * nu_sinh;

    TLorentzVector a1(px1, py1, pz1, sqrt(pT2_1 + pz1 * pz1));

    if (!TMath::IsNaN(a1.E()) )
      sol.push_back(a1);
    return std::move(sol);
  }

  ///-- 2 solutions case --///
  if(det > 0){
    double tmp   = sqrt(det) / (2. * C);
    double py1   = -D / (2. * C) + tmp;
    double py2   = -D / (2. * C) - tmp;
    double px1   = A * py1 + B;
    double px2   = A * py2 + B;
    double pT2_1 = px1 * px1 + py1 * py1;
    double pT2_2 = px2 * px2 + py2 * py2;
    double pz1   = sqrt(pT2_1) * nu_sinh;
    double pz2   = sqrt(pT2_2) * nu_sinh;
    TLorentzVector a1(px1, py1, pz1, sqrt(pT2_1 + pz1 * pz1));
    TLorentzVector a2(px2, py2, pz2, sqrt(pT2_2 + pz2 * pz2));

    if (!TMath::IsNaN(a1.E()) && !TMath::IsNaN(a2.E())){
      sol.push_back(a1);
      sol.push_back(a2);
    }
    return std::move(sol);
  }
  
  ///-- Should never reach this point --///
  return std::move(sol);
}


double NeutrinoWeighting::get_weight(TLorentzVector nu1, TLorentzVector nu2, double met_ex, double met_ey){

    double dx = met_ex - (nu1 + nu2).Px();
    double dy = met_ey - (nu2 + nu2).Py();

    double m_sigma_met_ex = 0;
    double m_sigma_met_ey = 0;

    if(flag_res_fixed && flag_res_dynamic_const_x_met){
      std::cout << "NW: You've asked for both fixed and dynamic resoltion. Ignoring fixed and giving you dynamic (FIX THIS!)" << std::endl;
    }

    if(flag_res_fixed){
      m_sigma_met_ex = res_fixed_value;
      m_sigma_met_ey = res_fixed_value;
    }
    if(flag_res_dynamic_const_x_met){
      m_sigma_met_ex = res_dynamic_const_x_met_value*met_ex;
      m_sigma_met_ey = res_dynamic_const_x_met_value*met_ey;
    }  

    double numerator_x = -dx*dx;
    double numerator_y = -dy*dy;

    double denominator_x = m_sigma_met_ex*m_sigma_met_ex;
    double denominator_y = m_sigma_met_ey*m_sigma_met_ey;

    double exp_x = exp(numerator_x/denominator_x);
    double exp_y = exp(numerator_y/denominator_y);

    return exp_x*exp_y;
}


TLorentzVector NeutrinoWeighting::Average(std::vector<TLorentzVector> vecs){

  double average_m = 0;
  TVector3 three_vecs = TVector3();
  TLorentzVector blank;
  if(vecs.size() == 0){
    return blank;
  }

  for(uint i = 0; i < vecs.size(); ++i){
    TVector3 tmp_vec = TVector3();
    tmp_vec.SetXYZ(vecs[i].X(), vecs[i].Y(), vecs[i].Z());
    average_m += vecs[i].M();
    three_vecs = three_vecs + tmp_vec;
  }

  three_vecs = three_vecs*(1./vecs.size());
  average_m = average_m/vecs.size();

  TLorentzVector new_vec = TLorentzVector();
  new_vec.SetPtEtaPhiM(three_vecs.Pt(), three_vecs.Eta(), three_vecs.Phi(), average_m);
  return new_vec;
}
