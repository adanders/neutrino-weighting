from ROOT import TCanvas, TFile, TTree, TH1D, gROOT, TLegend, TH1, kAzure, kMagenta, kRed, kOrange, THStack, kBlue, kGreen
import helper_atlas_labels as al
from array import array

TH1.SetDefaultSumw2()
gROOT.SetBatch(True)

AtlasStyle = al.atlasStyle()
AtlasStyle.SetPadTopMargin(    0.05)
AtlasStyle.SetPadRightMargin(  0.05)
AtlasStyle.SetPadBottomMargin( 0.2)
AtlasStyle.SetPadLeftMargin(   0.15)
gROOT.SetStyle("ATLAS")
gROOT.ForceStyle()


nbins = 20
binlow  = 0
binhigh = 1


COLOR_WT     = kBlue -4
COLOR_TTBAR  = kBlue -7
COLOR_BBTT   = kGreen +1
COLOR_BBZZ   = kMagenta-3
COLOR_BBWW   = kOrange-3
COLOR_DY     = kOrange
COLOR_DY_TAU = kRed

#input_path = "./2L_OS_2B_10Scans_Gauss_SMLepAssumption_NoSmearing_res2timesMet/"
input_path = "./2L_OS_2B_NoNW/"

BLIND   = True
UNBLIND = False

ALL   = 0
MC16A = 1
MC16D = 2
MC16E = 3

DATA_PERIOD = MC16D


weight_mc16a  = 0.26163359271513010  #NEW: (3244.54 + 33402.2)/140068.94 #OLD: 0.26043072825306840 
weight_mc16d  = 0.31863309595974665  #NEW: 44630.6/140068.94             #OLD: 0.31889050970488775
weight_mc16e  = 0.41973331132512315  #NEW: 58791.6/140068.94             #OLD: 0.42067876204204396

weight_to_use = 1
if DATA_PERIOD == MC16A:
	weight_to_use = weight_mc16a
if DATA_PERIOD == MC16D:
	weight_to_use = weight_mc16d
if DATA_PERIOD == MC16E:
	weight_to_use = weight_mc16e

xsec_bbWW     = (27.9/1000.)*0.499913
xsec_bbZZ     = (27.9/1000.)*0.2499991
xsec_bbtt     = (27.9/1000.)*0.5004637
xsec_tt       = 729.77*1.13975636159*0.1053700
xsec_Wt       = 3.9968*0.945*1.0
xsec_Wtbar    = 3.9940*0.946*1.0
xsec_Zee_Bf   = 1000*0.05539020352   #55.5413852
xsec_Zee_BvCf = 1000*0.2864521841    #286.392209
xsec_Zee_BvCv = 1000*1.879379956     #1879.37529
xsec_Zmm_Bf   = 1000*0.05418969795   #54.1530727
xsec_Zmm_BvCf = 1000*0.2870022143    #287.347368
xsec_Zmm_BvCv = 1000*1.880177188     #1879.99726
xsec_Ztt_Bf   = 1000*0.054000222    #6.68473707
xsec_Ztt_BvCf = 1000*0.2785761      #34.5071089
xsec_Ztt_BvCv = 1000*1.889070297     #234.126866


lumi_to_scale_to = 140*1000*weight_to_use


###############################################################################
###---                         Set up Binning                           ---####
###############################################################################

bins_NW = []
x1 = -1.05
while x1 < 1:
	bins_NW.append(x1)
	x1 = round(x1 + 0.05,2)
bins_NW = array('d', bins_NW)

bins_nbjets = array('d', [0, 1, 2, 3, 4, 5])
bins_dR     = array('d', [0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2, 3.4, 3.6, 3.8, 4.0])
bins_pt     = array('d', [0.0, 20., 40., 60., 80., 100., 120., 140., 160., 180., 200., 220., 240., 260., 280., 300.])
bins_low_pt = array('d', [5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0, 23.0, 24.0, 25.0, 26.0, 27.0, 28.0, 29.0, 30.0])
bins_met    = array('d', [0.0, 20., 40., 60., 80., 100., 120., 140., 160., 180., 200., 1000])
bins_dphi   = array('d', [-1, -0.8, -0.6, -0.4, -0.2, 0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
bins_absdphi   = array('d', [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0])
bins_eta    = []
bins_phi    = []
bins_m      = []
bins_m_large= []

x1 = 0
while x1 < 300:
	bins_m.append(x1)
	x1 = round(x1 + 20,2)
bins_m = array('d', bins_m)

x1 = 0
while x1 < 1000:
	bins_m_large.append(x1)
	x1 = round(x1 + 50,2)
bins_m_large = array('d', bins_m_large)


x1 = -2.5
while x1 < 2.5:
	bins_eta.append(x1)
	x1 = round(x1 + 0.1,2)
bins_eta = array('d', bins_eta)

x1 = -1
while x1 < 1:
	bins_phi.append(x1)
	x1 = round(x1 + 0.1,2)
bins_phi = array('d', bins_phi)



###############################################################################
###---                           Set up Observables                     ---####
###############################################################################

observables = [#["NW_weight",                   "NW_weight", "NW Weight",                  bins_NW],
               #["dRbb",                        "dR_bb",        "#Delta R(b1,b2)",              bins_dR],
               #["mbb",                         "mbb",          "M(bb) [Gev]",                  bins_m],
               #["pTbb",                        "pTbb",         "p_{T}(bb) [Gev]",              bins_pt],
               #["dPhibb/TMath::Pi()",          "dPhibb",       "#Delta#varphi(b1,b2) [rad]",   bins_dphi],               
               #["abs(dPhibb/TMath::Pi())",     "absdPhibb",    "|#Delta#varphi(b1,b2)| [rad]", bins_absdphi],                              
               #["dRll",                        "dR_ll",        "#Delta R(l+,l-)",              bins_dR],
               ["mll[0]",                       "mll",          "M(ll) [Gev]",                  bins_m],
               #["pTll",                        "pTll",         "p_{T}(ll) [Gev]",              bins_pt],
               #["dPhill/TMath::Pi()",          "dPhill",       "#Delta#varphi(l+,l-) [rad]",   bins_dphi], 
               #["abs(dPhill/TMath::Pi())",     "absdPhill",    "|#Delta#varphi(l+,l-)| [rad]", bins_absdphi],                
               #["dRbbll",                      "dR_bbll",      "#Delta R(bb,ll)",              bins_dR],
               #["mbbll",                       "mbbll",        "M(bbll) [Gev]",                bins_m_large],
               #["pTbbll",                      "pTbbll",       "p_{T}(bbll) [Gev]",            bins_pt],
               #["dPhibbll/TMath::Pi()",        "dPhibbll",     "#Delta#varphi(bb,ll) [rad]",   bins_dphi],      
               #["abs(dPhibbll/TMath::Pi())",   "absdPhibbll",  "|#Delta#varphi(bb,ll)| [rad]", bins_absdphi],                     
               #["met_NOSYS_met/1000.",         "MET",          "MET [GeV]",                    bins_met],
               #["lep_pt",                       "lep_pt",       "pT(l) [GeV]",                  bins_pt],               
               #["lep_pt",                       "lep_low_pt",   "pT(l) [GeV]",                  bins_low_pt],                              
               #["lep_pt[0]",                   "lep1_pt",      "pT(l1) [GeV]",                 bins_pt],
               #["lep_pt[1]",                   "lep2_pt",      "pT(l2) [GeV]",                 bins_pt],
               #["bjet_pt",                     "bjet_pt",      "pT(b) [GeV]",                  bins_pt],               
               #["bjet_pt[0]",                  "bjet1_pt",     "pT(b1) [GeV]",                 bins_pt],
               #["bjet_pt[1]",                  "bjet2_pt",     "pT(b2) [GeV]",                 bins_pt], 
               #["lep_eta",                     "lep_eta",      "#eta(l)",                      bins_eta], 
               #["bjet_eta",                    "bjet_eta",     "#eta(b)",                      bins_eta],                
               #["lep_phi/TMath::Pi()",         "lep_phi",      "#phi(l)",                      bins_phi], 
               #["bjet_phi/TMath::Pi()",        "bjet_phi",     "#phi(b)",                      bins_phi],                
               ]





#Tight cuts, not optimised on NW
cut_dphi_bbll = "abs(dPhibbll/TMath::Pi()) > 0.6" #0.5
cut_dR_bbll   = "dRbbll > 2" #1
cut_mll       = "mll < 70" #100
cut_mbb       = "mbb < 150 && mbb > 50"
cut_dR_ll     = "dRll < TMath::Pi()"
cut_dphi_bb   = "abs(dPhibb/TMath::Pi()) < 0.7"
cut_dR_bb     = "dRbb < 2" #TMath::Pi()
cut_dphi_ll   = "abs(dPhill/TMath::Pi()) < 0.5"
cut_pT_ll     = "1"
cut_pT_bb     = "1" #100
cut_mbll      = "1"
cut_pT_b      = "1"

#Cuts optimised with NW considered
cut_dphi_bbll = "abs(dPhibbll/TMath::Pi()) > 0.6"
cut_dR_bbll   = "dRbbll > 2"
cut_mll       = "mll < 70"
cut_mbb       = "mbb < 150 && mbb > 50"
cut_dR_ll     = "dRll < 2"
cut_dphi_bb   = "abs(dPhibb/TMath::Pi()) < 0.7"
cut_dR_bb     = "dRbb < 2" #this is quite sharp...
cut_dphi_ll   = "abs(dPhill/TMath::Pi()) < 0.5"
cut_pT_ll     = "1"
cut_pT_bb     = "1" #100
cut_mbll      = "1"
cut_pT_b      = "1"
#mbbll > 200

#Cuts optimised with NW considered and looking at the plots
cut_NW        = "NW_weight <0" 
cut_NW        = "1" 
cut_dphi_bbll = "abs(dPhibbll/TMath::Pi()) > 0.7"
cut_dR_bbll   = "dRbbll > 2.2"
cut_mll       = "mll < 70"
cut_mbb       = "mbb < 150 && mbb > 60"
#cut_mbb       = "1"
cut_dR_ll     = "dRll < 1.6"
cut_dphi_bb   = "abs(dPhibb/TMath::Pi()) < 0.6"
cut_dR_bb     = "dRbb < 2" #this is quite sharp...
cut_dphi_ll   = "abs(dPhill/TMath::Pi()) < 0.4"
cut_pT_ll     = "pTll > 40"
cut_pT_bb     = "pTbb > 100"
cut_mbll      = "mbbll > 250"
cut_pT_b      = "bjet_pt > 60"



if DATA_PERIOD == MC16A:
	data_period = "_mc20a"
if DATA_PERIOD == MC16D:
	data_period = "_mc20d"
if DATA_PERIOD == MC16E:
	data_period = "_mc20e"




selections = [#["1",                            "ll, 2b",                       "2l_2b",                    UNBLIND],
			  ["flag_channel[0] == 0 ",        "e^{+}e^{-}, 2b",               "ee_2b",                    UNBLIND],
			  #["flag_channel[0] == 1 ",        "#mu^{+}#mu^{-}, 2b",           "mm_2b",                    UNBLIND],			  
			  ["flag_channel[0] == 2",          "e^{#pm}#mu^{#mp}, 2b",         "em_2b",                    UNBLIND],			  			  			  
 			  #["NW_weight >0 ",                "e#mu, 2b, NW > 0",             "2l_2b_NW_pass",            UNBLIND], 			   			  
 			  #["NW_weight <0 ",                "e#mu, 2b, NW < 0",             "2l_2b_NW_fail",            UNBLIND], 			  
 			  #["NW_weight <0 && dRbb <2",      "e#mu, 2b, NW < 0, dR(bb) < 2", "2l_2b_NW_fail_dRbb_less2", UNBLIND],
 			  #["NW_weight <0 && dRbb <2 && mll < 80 && dRll < 1.4 && pTbb > 120 && dRbbll > 2 && mbbll > 250 && abs(dPhill/TMath::Pi()) < 0.4 && abs(dPhibbll/TMath::Pi()) > 0.6 && abs(dPhill/TMath::Pi()) < 0.6",  "e#mu, 2b, NW < 0, SR", "2l_2b_SR", UNBLIND],
 			  #["NW_weight <0 && " + cut_dphi_bbll + " && " + cut_mll + " && " + cut_dR_bbll + " && mbb < 170 && dRll < TMath::Pi() & dRll < TMath::Pi() && abs(dPhibbll/TMath::Pi()) > 0.8 && dRbb > TMath::Pi() && abs(dPhill/TMath::Pi()) < 0.5",  "e#mu, 2b, NW < 0, SR", "2l_2b_SR", UNBLIND], 			  
 			  #[cut_NW + " && " + cut_dphi_bbll + " && " + cut_mll + " && " + cut_dR_bbll + " && " + cut_mbb + " && " + cut_dR_ll + " && " + cut_dphi_bbll + " && " + cut_dR_bb + " && " + cut_dphi_ll + " && " + cut_pT_ll + " && " + cut_pT_bb + " && " + cut_mbll + " && " + cut_pT_b + " && " + cut_dphi_bb,  "e#mu, 2b, NW < 0, SR", "2l_2b_SR", UNBLIND], 			   			  
]

for selection in selections:
	for observable in observables:


		hist_bbWW     = TH1D("hist_bbWW"     + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])
		hist_bbZZ     = TH1D("hist_bbZZ"     + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])
		hist_bbtt     = TH1D("hist_bbtautau" + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])
		hist_tt       = TH1D("hist_tt"       + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])
		hist_Wt       = TH1D("hist_Wt"       + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])
		hist_Wtbar    = TH1D("hist_Wtbar"    + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])
		hist_Zee_Bf   = TH1D("hist_Zee_Bf"   + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])
		hist_Zee_BvCf = TH1D("hist_Zee_BvCf" + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])		
		hist_Zee_BvCv = TH1D("hist_Zee_BvCv" + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])				
		hist_Zmm_Bf   = TH1D("hist_Zmm_Bf"   + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])
		hist_Zmm_BvCf = TH1D("hist_Zmm_BvCf" + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])		
		hist_Zmm_BvCv = TH1D("hist_Zmm_BvCv" + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])	
		hist_Ztt_Bf   = TH1D("hist_Ztt_Bf"   + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])
		hist_Ztt_BvCf = TH1D("hist_Ztt_BvCf" + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])		
		hist_Ztt_BvCv = TH1D("hist_Ztt_BvCv" + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])
		hist_data     = TH1D("hist_data"     + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])			
		hist_data_15  = TH1D("hist_data_15"  + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])
		hist_data_16  = TH1D("hist_data_16"  + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])
		hist_data_17  = TH1D("hist_data_17"  + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])		
		hist_data_18  = TH1D("hist_data_18"  + observable[1] + selection[2], ";"+observable[2]+";Events", len(observable[3])-1, observable[3])		


		period_number_string = "1"
		if DATA_PERIOD == MC16A:
			period_number_string = "284500"
		if DATA_PERIOD == MC16D:
			period_number_string = "999999"			
		if DATA_PERIOD == MC16E:
			period_number_string = "310000"	

		bin_number = 1
		
		#file_bbWW     = TFile(input_path + "DiHiggs_bbWW_skimmed"+data_period+".root")
		#gROOT.cd()

		#file_bbZZ     = TFile(input_path + "DiHiggs_bbZZ_skimmed"+data_period+".root")
		#gROOT.cd()

		#file_bbtt     = TFile(input_path + "DiHiggs_bbtautau_skimmed"+data_period+".root")
		#gROOT.cd()

		file_tt       = TFile(input_path + "ttbar_dilep_skimmed"+data_period+".root")
		gROOT.cd()

		#file_Wt       = TFile(input_path + "Wt_dilep_skimmed"+data_period+".root")
		#gROOT.cd()

		#file_Wtbar    = TFile(input_path + "Wtbar_dilep_skimmed"+data_period+".root")
		#gROOT.cd()

		file_Zee_Bf   = TFile(input_path + "Zee_BFilter_skimmed"+data_period+".root")
		gROOT.cd()
		
		#file_Zee_BvCf = TFile(input_path + "Zee_BVetoCFilter_skimmed"+data_period+".root")
		#gROOT.cd()

		#file_Zee_BvCv = TFile(input_path + "Zee_BVetoCVeto_skimmed"+data_period+".root")
		#gROOT.cd()

		file_Zmm_Bf   = TFile(input_path + "Zmumu_BFilter_skimmed"+data_period+".root")
		gROOT.cd()

		#file_Zmm_BvCf = TFile(input_path + "Zmumu_BVetoCFilter_skimmed"+data_period+".root")
		#gROOT.cd()

		#file_Zmm_BvCv = TFile(input_path + "Zmumu_BVetoCVeto_skimmed"+data_period+".root")
		#gROOT.cd()

		file_Ztt_Bf   = TFile(input_path + "Ztautau_BFilter_skimmed"+data_period+".root")
		gROOT.cd()

		#file_Ztt_BvCf = TFile(input_path + "Ztautau_BVetoCFilter_skimmed"+data_period+".root")
		#gROOT.cd()

		#file_Ztt_BvCv = TFile(input_path + "Ztautau_BVetoCVeto_skimmed"+data_period+".root")
		#gROOT.cd()			
	


		#tree_bbWW     = file_bbWW.Get("AnalysisMiniTree")
		#tree_bbZZ     = file_bbZZ.Get("AnalysisMiniTree")
		#tree_bbtt     = file_bbtt.Get("AnalysisMiniTree")
		tree_tt       = file_tt.Get("AnalysisMiniTree")
		#tree_Wt       = file_Wt.Get("AnalysisMiniTree")	
		#tree_Wtbar    = file_Wtbar.Get("AnalysisMiniTree")	
		tree_Zee_Bf   = file_Zee_Bf.Get("AnalysisMiniTree")
		#tree_Zee_BvCf = file_Zee_BvCf.Get("AnalysisMiniTree")		
		#tree_Zee_BvCv = file_Zee_BvCv.Get("AnalysisMiniTree")				
		tree_Zmm_Bf   = file_Zmm_Bf.Get("AnalysisMiniTree")
		#tree_Zmm_BvCf = file_Zmm_BvCf.Get("AnalysisMiniTree")		
		#tree_Zmm_BvCv = file_Zmm_BvCv.Get("AnalysisMiniTree")	
		tree_Ztt_Bf   = file_Ztt_Bf.Get("AnalysisMiniTree")
		#tree_Ztt_BvCf = file_Ztt_BvCf.Get("AnalysisMiniTree")		
		##tree_Ztt_BvCv = file_Ztt_BvCv.Get("AnalysisMiniTree")	




		#period_number_string = "1"
		#if DATA_PERIOD == MC16A:
	    #	 period_number_string = "284500"
		#if DATA_PERIOD == MC16D:
		#	period_number_string = "999999"			
		#if DATA_PERIOD == MC16D:
	#		period_number_string = "310000"	

		#cutflow_bbWW     = file_bbWW.Get(      "CutBookkeeper_600465_"+period_number_string+"_NOSYS")
		#cutflow_bbZZ     = file_bbZZ.Get(      "CutBookkeeper_600469_"+period_number_string+"_NOSYS")
		#cutflow_bbtt     = file_bbtt.Get(      "CutBookkeeper_600467_"+period_number_string+"_NOSYS")
		cutflow_tt       = file_tt.Get(        "CutBookkeeper_410472_"+period_number_string+"_NOSYS")
		#cutflow_Wt       = file_Wt.Get(        "CutBookkeeper_410648_"+period_number_string+"_NOSYS")
		#cutflow_Wtbar    = file_Wtbar.Get(     "CutBookkeeper_410649_"+period_number_string+"_NOSYS")
		cutflow_Zee_Bf   = file_Zee_Bf.Get(   "CutBookkeeper_700320_"+period_number_string+"_NOSYS")
		#cutflow_Zee_BvCf = file_Zee_BvCf.Get( "CutBookkeeper_700321_"+period_number_string+"_NOSYS")
		#cutflow_Zee_BvCv = file_Zee_BvCv.Get( "CutBookkeeper_700322_"+period_number_string+"_NOSYS")
		cutflow_Zmm_Bf   = file_Zmm_Bf.Get(   "CutBookkeeper_700323_"+period_number_string+"_NOSYS")
		#cutflow_Zmm_BvCf = file_Zmm_BvCf.Get( "CutBookkeeper_700324_"+period_number_string+"_NOSYS")
		#cutflow_Zmm_BvCv = file_Zmm_BvCv.Get( "CutBookkeeper_700325_"+period_number_string+"_NOSYS")
		cutflow_Ztt_Bf   = file_Ztt_Bf.Get(    "CutBookkeeper_700792_"+period_number_string+"_NOSYS")
		#cutflow_Ztt_BvCf = file_Ztt_BvCf.Get(  "CutBookkeeper_700327_"+period_number_string+"_NOSYS")
		##cutflow_Ztt_BvCv = file_Ztt_BvCv.Get("CutBookkeeper_700328_"+period_number_string+"_NOSYS")



		bin_number = 1
		#nevents_bbWW     = cutflow_bbWW.GetBinContent(bin_number)
		#nevents_bbZZ     = cutflow_bbZZ.GetBinContent(bin_number)
		#nevents_bbtt     = cutflow_bbtt.GetBinContent(bin_number)
		nevents_tt       = cutflow_tt.GetBinContent(bin_number)
		#nevents_Wt       = cutflow_Wt.GetBinContent(bin_number)
		#nevents_Wtbar    = cutflow_Wtbar.GetBinContent(bin_number)
		nevents_Zee_Bf   = cutflow_Zee_Bf.GetBinContent(bin_number)
		#nevents_Zee_BvCf = cutflow_Zee_BvCf.GetBinContent(bin_number)
		#nevents_Zee_BvCv = cutflow_Zee_BvCv.GetBinContent(bin_number)
		nevents_Zmm_Bf   = cutflow_Zmm_Bf.GetBinContent(bin_number)
		#nevents_Zmm_BvCf = cutflow_Zmm_BvCf.GetBinContent(bin_number)
		#nevents_Zmm_BvCv = cutflow_Zmm_BvCv.GetBinContent(bin_number)
		nevents_Ztt_Bf   = cutflow_Ztt_Bf.GetBinContent(bin_number)
		#nevents_Ztt_BvCf = cutflow_Ztt_BvCf.GetBinContent(bin_number)
		##nevents_Ztt_BvCv = cutflow_Ztt_BvCv.GetBinContent(bin_number)

		#lumi_bbWW     = nevents_bbWW      /xsec_bbWW 
		#lumi_bbZZ     = nevents_bbZZ      /xsec_bbZZ 
		#lumi_bbtt     = nevents_bbtt      /xsec_bbtt 
		lumi_tt       = nevents_tt        /xsec_tt   
		#lumi_Wt       = nevents_Wt        /xsec_Wt   
		#lumi_Wtbar    = nevents_Wtbar     /xsec_Wtbar
		lumi_Zee_Bf   = nevents_Zee_Bf    /xsec_Zee_Bf
		#lumi_Zee_BvCf = nevents_Zee_BvCf  /xsec_Zee_BvCf
		#lumi_Zee_BvCv = nevents_Zee_BvCv  /xsec_Zee_BvCv
		lumi_Zmm_Bf   = nevents_Zmm_Bf    /xsec_Zmm_Bf
		#lumi_Zmm_BvCf = nevents_Zmm_BvCf  /xsec_Zmm_BvCf
		#lumi_Zmm_BvCv = nevents_Zmm_BvCv  /xsec_Zmm_BvCv
		lumi_Ztt_Bf   = nevents_Ztt_Bf    /xsec_Ztt_Bf
		#lumi_Ztt_BvCf = nevents_Ztt_BvCf  /xsec_Ztt_BvCf
		##lumi_Ztt_BvCv = nevents_Ztt_BvCv  /xsec_Ztt_BvCv

		#scale_bbWW      = lumi_to_scale_to / lumi_bbWW 
		#scale_bbZZ      = lumi_to_scale_to / lumi_bbZZ 
		#scale_bbtt      = lumi_to_scale_to / lumi_bbtt 
		scale_tt        = lumi_to_scale_to / lumi_tt   
		#scale_Wt        = lumi_to_scale_to / lumi_Wt   
		#scale_Wtbar     = lumi_to_scale_to / lumi_Wtbar
		scale_Zee_Bf    = lumi_to_scale_to / lumi_Zee_Bf
		#scale_Zee_BvCf  = lumi_to_scale_to / lumi_Zee_BvCf
		#scale_Zee_BvCv  = lumi_to_scale_to / lumi_Zee_BvCv
		scale_Zmm_Bf    = lumi_to_scale_to / lumi_Zmm_Bf
		#scale_Zmm_BvCf  = lumi_to_scale_to / lumi_Zmm_BvCf
		#scale_Zmm_BvCv  = lumi_to_scale_to / lumi_Zmm_BvCv
		scale_Ztt_Bf    = lumi_to_scale_to / lumi_Ztt_Bf
		#scale_Ztt_BvCf  = lumi_to_scale_to / lumi_Ztt_BvCf
		##scale_Ztt_BvCv  = lumi_to_scale_to / lumi_Ztt_BvCv

		selection_cuts = selection[0]

		#tree_bbWW.Project(     hist_bbWW.GetName(),     observable[0], selection_cuts)
		#tree_bbZZ.Project(     hist_bbZZ.GetName(),     observable[0], selection_cuts)
		#tree_bbtt.Project(     hist_bbtt.GetName(),     observable[0], selection_cuts)
		tree_tt.Project(       hist_tt.GetName(),       observable[0], selection_cuts)
		#tree_Wt.Project(       hist_Wt.GetName(),       observable[0], selection_cuts)
		#tree_Wtbar.Project(    hist_Wtbar.GetName(),    observable[0], selection_cuts)	
		tree_Zee_Bf.Project(   hist_Zee_Bf.GetName(),   observable[0], selection_cuts)	
		#tree_Zee_BvCf.Project( hist_Zee_BvCf.GetName(), observable[0], selection_cuts)			
		#tree_Zee_BvCv.Project( hist_Zee_BvCv.GetName(), observable[0], selection_cuts)
		tree_Zmm_Bf.Project(   hist_Zmm_Bf.GetName(),   observable[0], selection_cuts)	
		#tree_Zmm_BvCf.Project( hist_Zmm_BvCf.GetName(), observable[0], selection_cuts)			
		#tree_Zmm_BvCv.Project( hist_Zmm_BvCv.GetName(), observable[0], selection_cuts)	
		tree_Ztt_Bf.Project(   hist_Ztt_Bf.GetName(),   observable[0], selection_cuts)	
		#tree_Ztt_BvCf.Project( hist_Ztt_BvCf.GetName(), observable[0], selection_cuts)			
		##tree_Ztt_BvCv.Project( hist_Ztt_BvCv.GetName(), observable[0], selection_cuts)


		if DATA_PERIOD == MC16A:
			file_data_15  = TFile(input_path + "Data_15_skimmed_data.root")
			gROOT.cd()
			file_data_16  = TFile(input_path + "Data_16_skimmed_data.root")
			gROOT.cd()	
			tree_data_15  = file_data_15.Get("AnalysisMiniTree")	
			tree_data_16  = file_data_16.Get("AnalysisMiniTree")	
			tree_data_15.Project(  hist_data_15.GetName(),  observable[0], selection_cuts)
			tree_data_16.Project(  hist_data_16.GetName(),  observable[0], selection_cuts)		
			hist_data.Add(hist_data_15)
			hist_data.Add(hist_data_16)
		elif DATA_PERIOD == MC16D:
			file_data_17  = TFile(input_path + "Data_17_skimmed_data.root")
			gROOT.cd()
			tree_data_17  = file_data_17.Get("AnalysisMiniTree")	
			tree_data_17.Project(  hist_data_17.GetName(),  observable[0], selection_cuts)		
			hist_data.Add(hist_data_17)
		elif DATA_PERIOD == MC16E:
			file_data_18  = TFile(input_path + "Data_18_skimmed_data.root")
			gROOT.cd()
			tree_data_18  = file_data_18.Get("AnalysisMiniTree")	
			tree_data_18.Project(  hist_data_18.GetName(),  observable[0], selection_cuts)		
			hist_data.Add(hist_data_18)
		else:
			print("Not supported yet")



		#Scale everything to lumi

		#hist_bbWW.Scale(     scale_bbWW)
		#hist_bbZZ.Scale(     scale_bbZZ)
		#hist_bbtt.Scale(     scale_bbtt)
		hist_tt.Scale(       scale_tt)
		#hist_Wt.Scale(       scale_Wt)
		#hist_Wtbar.Scale(    scale_Wtbar)
		hist_Zee_Bf.Scale(   scale_Zee_Bf)
		#hist_Zee_BvCf.Scale( scale_Zee_BvCf)		
		#hist_Zee_BvCv.Scale( scale_Zee_BvCv)		
		hist_Zmm_Bf.Scale(   scale_Zmm_Bf)
		#hist_Zmm_BvCf.Scale( scale_Zmm_BvCf)		
		#hist_Zmm_BvCv.Scale( scale_Zmm_BvCv)		
		hist_Ztt_Bf.Scale(   scale_Ztt_Bf)
		#hist_Ztt_BvCf.Scale( scale_Ztt_BvCf)		
		##hist_Ztt_BvCv.Scale( scale_Ztt_BvCv)		

		#print("hist_bbWW     ",hist_bbWW.Integral())
		#print("hist_bbZZ     ",hist_bbZZ.Integral())
		#print("hist_bbtt     ",hist_bbtt.Integral())
		print("hist_tt       ",hist_tt.Integral())
		#print("hist_Wt       ",hist_Wt.Integral())
		#print("hist_Wtbar    ",hist_Wtbar.Integral())
		print("hist_Zee_Bf   ",hist_Zee_Bf.Integral())
		#print("hist_Zee_BvCf ",hist_Zee_BvCf.Integral())		
		#print("hist_Zee_BvCv ",hist_Zee_BvCv.Integral())		
		print("hist_Zmm_Bf   ",hist_Zmm_Bf.Integral())
		#print("hist_Zmm_BvCf ",hist_Zmm_BvCf.Integral())		
		#print("hist_Zmm_BvCv ",hist_Zmm_BvCv.Integral())	
		print("hist_Ztt_Bf   ",hist_Ztt_Bf.Integral())
		#print("hist_Ztt_BvCf ",hist_Ztt_BvCf.Integral())		
		##print("hist_Ztt_BvCv ",hist_Ztt_BvCv.Integral())	
		print("hist_data     ",hist_data.Integral())



		### Add together DY ###
		#hist_Zee_Bf.Add(hist_Zee_BvCf)
		#hist_Zee_Bf.Add(hist_Zee_BvCv)
		#hist_Zmm_Bf.Add(hist_Zmm_BvCf)
		#hist_Zmm_Bf.Add(hist_Zmm_BvCv)
		#hist_Ztt_Bf.Add(hist_Ztt_BvCf)
		##hist_Ztt_Bf.Add(hist_Ztt_BvCv)

			


		stack = THStack("stack","")
		#hist_Wt.Add(hist_Wtbar)
		#stack.Add(hist_Wt)
		stack.Add(hist_tt)
		stack.Add(hist_Ztt_Bf)		
		stack.Add(hist_Zee_Bf)
		stack.Add(hist_Zmm_Bf)				
		#stack.Add(hist_bbtt)
		#stack.Add(hist_bbZZ)
		#stack.Add(hist_bbWW)


		#hist_Wt.Add(hist_tt)
		#hist_Wt.Add(hist_Ztt_Bf)		
		#hist_Wt.Add(hist_Zee_Bf)				
		#hist_Wt.Add(hist_Zmm_Bf)	
		#print(hist_Wt.GetBinContent(5)/hist_data.GetBinContent(5))
		#print(hist_Wt.GetBinContent(2)/hist_data.GetBinContent(2))		
		#input()

		hist_Wt.SetFillColor(COLOR_WT)
		hist_tt.SetFillColor(COLOR_TTBAR)
		hist_bbtt.SetFillColor(COLOR_BBTT)
		hist_bbZZ.SetFillColor(COLOR_BBZZ)
		hist_bbWW.SetFillColor(COLOR_BBWW)
		hist_Zee_Bf.SetFillColor(COLOR_DY)
		hist_Zmm_Bf.SetFillColor(COLOR_DY)	
		hist_Ztt_Bf.SetFillColor(COLOR_DY_TAU)	

		hist_Wt.SetLineColor(COLOR_WT)
		hist_tt.SetLineColor(COLOR_TTBAR)
		hist_bbtt.SetLineColor(COLOR_BBTT)
		hist_bbZZ.SetLineColor(COLOR_BBZZ)
		hist_bbWW.SetLineColor(COLOR_BBWW)
		hist_Zee_Bf.SetLineColor(COLOR_DY)
		hist_Zmm_Bf.SetLineColor(COLOR_DY)	
		hist_Ztt_Bf.SetLineColor(COLOR_DY_TAU)	

		hist_Wt.SetLineWidth(0)
		hist_tt.SetLineWidth(0)
		hist_bbtt.SetLineWidth(0)
		hist_bbZZ.SetLineWidth(0)
		hist_bbWW.SetLineWidth(0)
		hist_Zee_Bf.SetLineWidth(0)
		hist_Zmm_Bf.SetLineWidth(0)
		hist_Ztt_Bf.SetLineWidth(0)


		legend = TLegend(0.7,0.7,0.90,0.92)
		legend.AddEntry(hist_tt,     "t#bar{t} (dilep)",      "F")
		#legend.AddEntry(hist_Wt,     "Wt (dilep)",            "F")
		legend.AddEntry(hist_Zee_Bf, "Z#rightarrow ll",       "F")
		legend.AddEntry(hist_Ztt_Bf, "Z#rightarrow #tau#tau", "F")		
		#legend.AddEntry(hist_bbWW,   "HH (bbWW)",             "F")
		#legend.AddEntry(hist_bbZZ,   "HH (bbZZ)",             "F")
		#legend.AddEntry(hist_bbtt,   "HH (bb#tau#tau)",       "F")
		legend.SetLineColor(0)
		legend.SetFillColor(0)
		legend.SetTextSize(0.03)


		c1 = TCanvas()

		stack.Draw("HIST")
		stack.GetXaxis().SetTitle(observable[2])
		stack.GetYaxis().SetTitle("Events")	
		stack.SetMaximum(stack.GetMaximum()*1.4)

		if not selection[3]: #Check if selection is blinded
			hist_data.Draw("SAME")

		legend.Draw("SAME")

		#if observable[0] == "mbb":
	#		c1.SetLogy()

		legend_header = selection[1]
		lumi_string = "#sqrt{s} = 13 TeV, 140 fb^{-1}"

		if DATA_PERIOD == MC16A:
			lumi_string = "#sqrt{s} = 13 TeV, 36 fb^{-1}"
		if DATA_PERIOD == MC16D:
			lumi_string = "#sqrt{s} = 13 TeV, 44 fb^{-1}"						
		if DATA_PERIOD == MC16E:
			lumi_string = "#sqrt{s} = 13 TeV, 59 fb^{-1}"			

		al.make_ATLAS_label( 0.2, 0.89,  1, c1, 0.035, "  Internal")
		al.make_ATLAS_string( 0.2, 0.84, lumi_string, 0.035)
		al.make_ATLAS_string( 0.2, 0.79, legend_header, 0.035)


		c1.Print(input_path + "/Plots/DataMC_" + selection[2] +"_"+ observable[1] + ".pdf")
