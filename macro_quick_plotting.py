from ROOT import TCanvas, TFile, TTree, TH1D, gROOT, TLegend, TH1, kAzure, kMagenta, kRed, kOrange
import helper_atlas_labels as al
from array import array

TH1.SetDefaultSumw2()
gROOT.SetBatch(True)

AtlasStyle = al.atlasStyle()
AtlasStyle.SetPadTopMargin(    0.05)
AtlasStyle.SetPadRightMargin(  0.05)
AtlasStyle.SetPadBottomMargin( 0.2)
AtlasStyle.SetPadLeftMargin(   0.15)
gROOT.SetStyle("ATLAS")
gROOT.ForceStyle()


nbins = 20
binlow  = 0
binhigh = 1

color_bbWW = kAzure-3
color_bbtt = kMagenta-3
color_bbZZ = kOrange-3

bins = array('d', [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.9, 1.0])


bins = []
x1 = -1.05

while x1 < 1:
	bins.append(x1)
	x1 = round(x1 + 0.05,2)

bins = array('d', bins)
print(bins)
#hist_bbWW = TH1D("hist_bbWW",     ";Neutrino Weight; Arbitrary Units", nbins, binlow, binhigh)
#hist_bbZZ = TH1D("hist_bbZZ",     ";Neutrino Weight; Arbitrary Units", nbins, binlow, binhigh)
#hist_bbtt = TH1D("hist_bbtautau", ";Neutrino Weight; Arbitrary Units", nbins, binlow, binhigh)
#hist_tt   = TH1D("hist_tt",       ";Neutrino Weight; Arbitrary Units", nbins, binlow, binhigh)

hist_bbWW = TH1D("hist_bbWW",     ";Neutrino Weight; Arbitrary Units", len(bins)-1, bins)
hist_bbZZ = TH1D("hist_bbZZ",     ";Neutrino Weight; Arbitrary Units", len(bins)-1, bins)
hist_bbtt = TH1D("hist_bbtautau", ";Neutrino Weight; Arbitrary Units", len(bins)-1, bins)
hist_tt   = TH1D("hist_tt",       ";Neutrino Weight; Arbitrary Units", len(bins)-1, bins)

#input_path = "./2L_OS_2B_10Scans_Gauss_SMLepAssumption_NoSmearing_res1timesMet/"
input_path = "./2L_OS_2B_10Scans_Gauss_SMLepAssumption_NoSmearing_res2timesMet/"
#input_path = "./2L_OS_2B_10Scans_Gauss_SMLepAssumption_NoSmearing_res4timesMet/"
#input_path = "./2L_OS_2B_10Scans_Gauss_SMLepAssumption_NoSmearing_res10/"

file_bbWW = TFile(input_path + "DiHiggs_bbWW.root")
gROOT.cd()
file_bbZZ = TFile(input_path + "DiHiggs_bbZZ.root")
gROOT.cd()
file_bbtt = TFile(input_path + "DiHiggs_bbtautau.root")
gROOT.cd()
file_tt   = TFile(input_path + "ttbar_dilep_skimmed.root")
gROOT.cd()

tree_bbWW = file_bbWW.Get("AnalysisMiniTree")
tree_bbZZ = file_bbZZ.Get("AnalysisMiniTree")
tree_bbtt = file_bbtt.Get("AnalysisMiniTree")
tree_tt   = file_tt.Get("AnalysisMiniTree")

tree_bbWW.Project(hist_bbWW.GetName(), "NW_weight", "NW_weight > -1.5")
tree_bbZZ.Project(hist_bbZZ.GetName(), "NW_weight", "NW_weight > -1.5")
tree_bbtt.Project(hist_bbtt.GetName(), "NW_weight", "NW_weight > -1.5")
tree_tt.Project(  hist_tt.GetName(),   "NW_weight", "NW_weight > -1.5")

hist_bbtt.SetLineWidth(3)
hist_bbZZ.SetLineWidth(3)
hist_bbWW.SetLineWidth(3)
hist_tt.SetLineWidth(3)

hist_bbWW.SetLineColor(color_bbWW)
hist_bbZZ.SetLineColor(color_bbZZ)
hist_bbtt.SetLineColor(color_bbtt)
hist_tt.SetLineColor(1)

hist_bbWW.SetMarkerColor(7)
hist_bbZZ.SetMarkerColor(2)
hist_bbtt.SetMarkerColor(4)
hist_tt.SetMarkerColor(1)

print("hist_bbWW efficiency:", hist_bbWW.GetBinContent(2)/hist_bbWW.GetEntries())
print("hist_bbtt efficiency:", hist_bbtt.GetBinContent(2)/hist_bbtt.GetEntries())
print("hist_bbZZ efficiency:", hist_bbZZ.GetBinContent(2)/hist_bbZZ.GetEntries())
print("hist_tt   efficiency:", hist_tt.GetBinContent(2)/hist_tt.GetEntries())

print("hist_bbWW efficiency:", hist_bbWW.GetBinContent(2)/hist_bbWW.Integral())
print("hist_bbtt efficiency:", hist_bbtt.GetBinContent(2)/hist_bbtt.Integral())
print("hist_bbZZ efficiency:", hist_bbZZ.GetBinContent(2)/hist_bbZZ.Integral())
print("hist_tt   efficiency:", hist_tt.GetBinContent(2)/hist_tt.Integral())

hist_bbWW.Scale(1./hist_bbWW.Integral())
hist_bbZZ.Scale(1./hist_bbZZ.Integral())
hist_bbtt.Scale(1./hist_bbtt.Integral())
hist_tt.Scale(1./hist_tt.Integral())

#hist_bbWW.Divide(hist_tt)
#hist_bbZZ.Divide(hist_tt)
#hist_bbtt.Divide(hist_tt)
#hist_tt.Divide(hist_tt)

legend = TLegend(0.7,0.7,0.90,0.92)
legend.AddEntry(hist_tt,   "t#bar{t} (dilep)", "L")
legend.AddEntry(hist_bbWW, "HH (bbWW)",        "L")
legend.AddEntry(hist_bbZZ, "HH (bbZZ)",        "L")
legend.AddEntry(hist_bbtt, "HH (bb#tau#tau)",  "L")
legend.SetLineColor(0)
legend.SetFillColor(0)
legend.SetTextSize(0.03)

hist_bbWW.SetMaximum(15)
#hist_bbWW.SetMaximum(hist_tt.GetMaximum()*1.2)
c1 = TCanvas()
c1.SetLogy()
hist_bbWW.Draw("HIST")
hist_bbZZ.Draw("HIST SAME")
hist_bbtt.Draw("HIST SAME")
hist_tt.Draw("HIST SAME")

legend.Draw("SAME")

legend_header = "2L OS, 2b"
al.make_ATLAS_label( 0.2, 0.87,  1, c1, 0.035, "  Internal Simulation")
al.make_ATLAS_string( 0.2, 0.83, legend_header, 0.035)


c1.Print(input_path + "Weight_comparison.pdf")
