# Neutrino Weighting

This repository is a standalone implementation of Neutrino Weighting (NW) that is designed to run via a python script on NTuples produced by either the EasyJet DiHiggs framework or TopCPToolkit. It is not designed for speed and shouldn't be used for large-scale implementation of NW.

[[_TOC_]]

## Brief explanation of Neutrino Weighting

Jay, write this

## Setting up and compiling macro_skim.py

In order to run the skim macro you need to have downloaded some NTuples that you want to skim that are in the bbll NTuple format and to have compiled the NW code. 

To compile the code simply open root and use the in-line compiler

```
root -l
.L NeutrinoWeighting.cxx++
```

This should create a library for NW which is then loaded by macro_skim.py 

```
gROOT.ProcessLine(".L NeutrinoWeighting_cxx.so")
```

If you make any changes to the NW code, you will need to recompile it in order for python to pick up the changes.

## NTuples

The NTuple directory structure that the code expects by default can be seen in macro_skim.py. Essentially, it expects a top level directory called "NTuples" in the directory where macro_skim.py is run, and then users can specify sub-directories (such as DiHiggs and ttbar). You can change this if you want, but make sure you update macro_skim.py to point to wherever your files are stored. 

### Getting DiHiggs NTuples

A set of NTuples exists in Giuseppe's cernbox directory here: Here are the ntuples: https://cernbox.cern.ch/files/spaces/eos/user/g/gcallea/NTUP_forbbllteam


### Getting Top NTuples

You can't, Jay hasn't made any yet.


## Configuring Neutrino Weighting

There are many different configuration options for NW, most of which can be set without re-compiling the code itself. The most common options to change are listed below.

### Eta Sampling

The are a number of ways that NW can sample the two neutrino pseudo-rapidities, as well as the number of sampling points that it takes. The options for the ways that NW samples the pseudorapidity spaces are:
- Linear: A flat sampling PDF between -5 and +5
- Gauss: A gaussian distribution PDF with mean 0 and sigma of 1
- SMLep: A gaussian PDF centered at $`0.72 x eta(lep) - 0.018 x eta(lep)^3`$ (based on the relationship in the SM between the lep and nu in W decays from tops taken from ttbar MC.) Switching between these options is controlled via the following functions:
```
  void SetEtaSamplingLinear(bool)
  void SetEtaSamplingSMLep(bool)
  void SetEtaSamplingGauss(bool)
```

You need to set the number of sample points (defualt is 2) using:
```
  void SetEtaSamplingNsamples(int)
```

In addition, one can sample from these PDFs in equidistant steps, or randomly using TRandom3. 

Linear sampling is currently only defined for flat eta sampling and by default runs between -5 and +5. If you want to change this range, you can do so with the following options.
```
  void SetEtaSamplingLinearStepLow(float)
  void SetEtaSamplingLinearStepHigh(float)
```
and the code will then determine the sample points using high-low/samples. 

Random sampling is allowed for any option and will simply use the relevant PDF and the number of sample points. When using the random option, you should set the random seed to something unique each event (e.g. the event number)
```
void SetRandomSeed(int)
```

### Resolution used in weight calculation

The weight calculated for each solution has the following form:

![NW weight formula](images/NW_weight.png)

where $sigma_x/y$ is the resolution of the MET. This value can either be set to something fixed (e.g. 10 GeV) or it can be dynamically changed for each event. Two options are currently supported; a fixed value and a dynamic constant value * MET observed in the event.

The fixed value (by default 10 GeV) is set using the following:
```
void SetFlagResFixed(bool)
void SetResFixedValue(float)
```
whereas the dynamic (const x MET, const default=2) is set using:
```
void SetFlagResDynamicConstXMet(bool)
void SetResConstXMetValue(float)
```
Note that the dynamic option treats the x and y directions seperately (i..e it will use the MET in the x direction and the MET in the y direction) whereas the fixed value is the same for both.

If you want to change to a different dynamic value event-by-event, you can use the fixed value option and set it each event in the python (but it will be the same for the x and y direction).

### Stopping criteria

In some cases, it can be useful to simply stop NW after it has found at least one real solution for an events (and not to continue scanning the rest of the eta space). Generally, this is only used to make the code faster and will not result in the best-possible-solutuion. To turn this option on (by default, it is off), use:
```
void StopAfterFirstSolution(bool)
```

### Weight threshold

Sometimes, you may wish to consider very small weights as failed solutions. To do this, you can set a threshold for what weight NW will consider as successful (by default, this value is set to 0)
```
void SetWeightThreshold(double)
```
**CAUTION: Use this option only if you've already optimised your resolution in the weight calculation. If you choose a poor resolution you could get lots of very small weights and then this option will kill your efficiency.**




