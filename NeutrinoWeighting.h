#ifndef NeutrinoWeighting_h
#define NeutrinoWeighting_h
#include <iostream>

#include "TObject.h"
#include "TRandom3.h"
#include "TLorentzVector.h"
#include <assert.h>
#include "Math/VectorUtil.h"
#include <Math/Polynomial.h>
#include <TMatrix.h>
#include <TMatrixD.h>
#include <TArrayD.h>
#include <TMatrixDEigen.h>
#include <TMath.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <stdio.h>
#include <stdlib.h>
#include <TVector3.h>
#include "TH1F.h"

//namespace top{

class NeutrinoWeighting {

private:

  std::vector<TLorentzVector> m_tops;
  std::vector<TLorentzVector> m_tbars;  
  std::vector<TLorentzVector> m_nus;
  std::vector<TLorentzVector> m_nubars;  
  std::vector<TLorentzVector> m_Wposs;
  std::vector<TLorentzVector> m_Wnegs;  

  std::vector<double> m_weights;

  TLorentzVector m_highestWeightTop;
  TLorentzVector m_highestWeightTbar;
  TLorentzVector m_highestWeightNu;
  TLorentzVector m_highestWeightNubar;
  TLorentzVector m_highestWeightWpos;
  TLorentzVector m_highestWeightWneg;

  TRandom3 m_random;

  bool flag_eta_sampling_linear; // sampling in equal step sizes
  bool flag_eta_sampling_SM_lep; // sampling using lep-nu eta SM relationship
  bool flag_eta_sampling_gauss;  // sampling using a user-defined gaussian
  bool flag_eta_sampling_do_random;  // sample using random numbers, not for linear

  float eta_sampling_linear_step_low;
  float eta_sampling_linear_step_high;
  float eta_sampling_gauss_mean;
  float eta_sampling_gauss_sigma;

  bool flag_res_fixed;
  bool flag_res_dynamic_const_x_met;
  float res_fixed_value;
  float res_dynamic_const_x_met_value;

  uint eta_sampling_nsamples; // how many sample points to do 

  std::vector<float> m_eta_points_nu;
  std::vector<float> m_eta_points_nubar;  

  double highestWeight;
  double m_weight_threshold;
  bool stop_after_first_solution;

public:

  virtual ~NeutrinoWeighting();
  NeutrinoWeighting();

  void Reconstruct(TLorentzVector lepton_pos, 
                   TLorentzVector lepton_neg, 
                   TLorentzVector b, 
                   TLorentzVector bbar, 
                   double met_ex, 
                   double met_ey, 
                   double mtop,
                   double mtbar,
                   double mWpos,
                   double mWneg);

  double get_weight(TLorentzVector nu1, 
                    TLorentzVector nu2, 
                    double met_ex, 
                    double met_ey);

  void Reset();
  void SetupEtaSampling(); // sets up sampling points based on settings

  std::vector<TLorentzVector> solveForNeutrinoEta(TLorentzVector* lepton, 
                                                  TLorentzVector* bJet, 
                                                  double nu_eta, 
                                                  double mtop, 
                                                  double mW);

  ///-- Getters for objects --///
  std::vector<TLorentzVector> GetTops(){  return m_tops;};
  std::vector<TLorentzVector> GetTbars(){ return m_tbars;};
  std::vector<TLorentzVector> GetNus(){   return m_nus;};
  std::vector<TLorentzVector> GetNubars(){return m_nubars;};
  std::vector<TLorentzVector> GetWposs(){ return m_Wposs;};
  std::vector<TLorentzVector> GetWnegs(){ return m_Wnegs;};
  TLorentzVector              GetTop(){   return m_highestWeightTop;};
  TLorentzVector              GetTbar(){  return m_highestWeightTbar;};
  TLorentzVector              GetNu(){    return m_highestWeightNu;};
  TLorentzVector              GetNubar(){ return m_highestWeightNubar;};
  TLorentzVector              GetWpos(){  return m_highestWeightWpos;};
  TLorentzVector              GetWneg(){  return m_highestWeightWneg;};


  ///-- Functions Related to weights --///
  std::vector<double> GetWeights(){return m_weights;};
  double              GetWeight(){ return highestWeight;};
  void SetWeightThreshold(double choice=0.0){m_weight_threshold=choice;};


  ///-- Setters for sampling options --///
  void SetEtaSamplingLinear(          bool choice=true){ flag_eta_sampling_linear     = choice; flag_eta_sampling_SM_lep= false; flag_eta_sampling_gauss = false;};
  void SetEtaSamplingSMLep(           bool choice=true){ flag_eta_sampling_SM_lep     = choice; flag_eta_sampling_linear= false; flag_eta_sampling_gauss = false;};
  void SetEtaSamplingGauss(           bool choice=true){ flag_eta_sampling_gauss      = choice; flag_eta_sampling_linear= false; flag_eta_sampling_SM_lep= false;};
  void SetEtaSamplingDoRandom(        bool choice=true){ flag_eta_sampling_do_random  = choice;};
  void SetEtaSamplingLinearStepLow(  float choice=-5.0){ eta_sampling_linear_step_low = choice;};
  void SetEtaSamplingLinearStepHigh( float choice= 5.0){ eta_sampling_linear_step_low = choice;};
  void SetEtaSamplingGaussMean(      float choice= 0.0){ eta_sampling_gauss_mean      = choice;};
  void SetEtaSamplingGaussSigma(     float choice= 1.0){ eta_sampling_gauss_sigma     = choice;};  
  void SetEtaSamplingNsamples( int number=2){eta_sampling_nsamples = number;};
  void SetupEtaSampling(TLorentzVector, TLorentzVector);

  void SetFlagResFixed(            bool choice=false){ flag_res_fixed                = choice;};
  void SetFlagResDynamicConstXMet( bool choice=false){ flag_res_dynamic_const_x_met  = choice;};
  void SetResFixedValue(          float choice=10){    res_fixed_value               = choice;};
  void SetResConstXMetValue(      float choice=2){     res_dynamic_const_x_met_value = choice;};


  ///-- Other helper functions --///
  TLorentzVector Average(std::vector<TLorentzVector> vecs);
  void SetRandomSeed(int seed){m_random.SetSeed(seed);};
  void StopAfterFirstSolution(bool choice=false){stop_after_first_solution = choice;};
};

#endif
 
 

