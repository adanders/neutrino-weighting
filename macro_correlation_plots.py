from ROOT import TCanvas, TFile, TTree, TH1D, gROOT, TLegend, TH1, kAzure, kMagenta, kRed, kOrange, THStack, kBlue, kGreen, TH2D
import helper_atlas_labels as al
from array import array

TH1.SetDefaultSumw2()
gROOT.SetBatch(True)

AtlasStyle = al.atlasStyle()
AtlasStyle.SetPadTopMargin(    0.05)
AtlasStyle.SetPadRightMargin(  0.05)
AtlasStyle.SetPadBottomMargin( 0.2)
AtlasStyle.SetPadLeftMargin(   0.15)
gROOT.SetStyle("ATLAS")
gROOT.ForceStyle()


nbins = 20
binlow  = 0
binhigh = 1


COLOR_WT    = kBlue -4
COLOR_TTBAR = kBlue -7
COLOR_BBTT  = kGreen +1
COLOR_BBZZ  = kMagenta-3
COLOR_BBWW  = kOrange-3

input_path = "./2L_OS_2B_10Scans_Gauss_SMLepAssumption_NoSmearing_res2timesMet/"

dphi_bb = "dPhibb/TMath::Pi()"
abs_dphi_bb = "dPhibb/TMath::Pi()"

dphi_ll = "dPhill/TMath::Pi()"
abs_dphi_ll = "dPhill/TMath::Pi()"

dphi_bbll = "dPhibbll/TMath::Pi()"
abs_dphi_bbll = "dPhibbll/TMath::Pi()"

observables1D = [["dRbb",        "dR_bb",         "#Delta R(b1,b2)",        100,  0.,   6.],
                 ["mbb",         "mbb",           "M(bb) [Gev]",            100,  0., 300.],
			     ["pTbb",        "pT_bb",         "p_T(bb) [Gev]",          100,  0., 500.],
				 [abs_dphi_bb,   "abs_dPhi_bb",   "|dPhi(bb)| [rad]",       100,  0.,   1.],
				 ["dRll",        "dR_ll",         "#Delta R(l+,l-)",        100,  0.,   6.],
				 ["mll",         "mll",           "M(ll) [Gev]",            100,  0., 300.],
				 ["pTll",        "pT_ll",         "p_T(ll) [Gev]",          100,  0., 500.],
				 [abs_dphi_ll,   "abs_dPhi_ll",   "|dPhi(ll)| [rad]",       100,  0.,   1.],
				 ["dRbbll",        "dR_bbll",         "#Delta R(bb,ll)",    100,  0.,   6.],
				 ["mbbll",         "mbbll",           "M(bbll) [Gev]",      100,  0., 700.],
				 ["pTbbll",        "pT_bbll",         "p_T(bbll) [Gev]",    100,  0., 500.],
				 [abs_dphi_bbll,   "abs_dPhi_bbll",   "|dPhi(bbll)| [rad]", 100,  0.,   1.],
]
observables1D = []

observables2D = [["dRbb",   "dR_bb",   "#Delta R(b1,b2)", 100, 0.,   6., "mbb",         "mbb",           "M(bb) [Gev]",      100,  0., 300.],
			     ["dRbb",   "dR_bb",   "#Delta R(b1,b2)", 100, 0.,   6., "pTbb",        "pT_bb",         "p_T(bb) [Gev]",    100,  0., 500.],
			     ["dRbb",   "dR_bb",   "#Delta R(b1,b2)", 100, 0.,   6., abs_dphi_bb,   "abs_dPhi_bb",   "|dPhi(bb)| [rad]", 100,  0.,   1.],
			     ["mbb",    "mbb",     "M(bb) [Gev]",     100, 0., 300., "pTbb",        "pT_bb",         "p_T(bb) [Gev]",    100,  0., 500.],
			     ["mbb",    "mbb",     "M(bb) [Gev]",     100, 0., 300., abs_dphi_bb,   "abs_dPhi_bb",   "|dPhi(bb)| [rad]", 100,  0.,   1.],			   
			     ["pTbb",   "pT_bb",   "p_T(bb) [Gev]",   100, 0., 500., abs_dphi_bb,   "abs_dPhi_bb",   "|dPhi(bb)| [rad]", 100,  0.,   1.],		
			     ["dRll",   "dR_ll",   "#Delta R(l+,l-)", 100, 0.,   6., "mll",         "mll",           "M(ll) [Gev]",      100,  0., 300.],
			     ["dRll",   "dR_ll",   "#Delta R(l+,l-)", 100, 0.,   6., "pTll",        "pT_ll",         "p_T(ll) [Gev]",    100,  0., 500.],
			     ["dRll",   "dR_ll",   "#Delta R(l+,l-)", 100, 0.,   6., abs_dphi_ll,   "abs_dPhi_ll",   "|dPhi(ll)| [rad]", 100,  0.,   1.],
			     ["mll",    "mll",     "M(ll) [Gev]",     100, 0., 300., "pTll",        "pT_ll",         "p_T(ll) [Gev]",    100,  0., 500.],
			     ["mll",    "mll",     "M(ll) [Gev]",     100, 0., 300., abs_dphi_ll,   "abs_dPhi_ll",   "|dPhi(ll)| [rad]", 100,  0.,   1.],			   
			     ["pTll",   "pT_ll",   "p_T(ll) [Gev]",   100, 0., 500., abs_dphi_ll,   "abs_dPhi_ll",   "|dPhi(ll)| [rad]", 100,  0.,   1.],			   			   			   
			     ["dRbbll", "dR_bbll", "#Delta R(bb,ll)", 100, 0.,   6., "mbbll",       "mbbll",         "M(bbll) [Gev]",       100,  0., 500.],
			     ["dRbbll", "dR_bbll", "#Delta R(bb,ll)", 100, 0.,   6., "pTbbll",      "pT_bbll",       "p_T(bbll) [Gev]",     100,  0., 700.],
			     ["dRbbll", "dR_bbll", "#Delta R(bb,ll)", 100, 0.,   6., abs_dphi_bbll, "abs_dPhi_bbll", "|dPhi(bb,ll)| [rad]", 100,  0.,   1.],
			     ["mbbll",  "mbbll",   "M(bbll) [Gev]",   100, 0., 500., "pTbbll",      "pT_bbll",       "p_T(bbll) [Gev]",     100,  0., 700.],
			     ["mbbll",  "mbbll",   "M(bbll) [Gev]",   100, 0., 500., abs_dphi_bbll, "abs_dPhi_bbll", "|dPhi(bb,ll)| [rad]", 100,  0.,   1.],			   
			     ["pTbbll", "pT_bbll", "p_T(bbll) [Gev]", 100, 0., 700., abs_dphi_bbll, "abs_dPhi_bbll", "|dPhi(bb,ll)| [rad]", 100,  0.,   1.],	
			     #["lep_pt[0]", "pT_l1", "pT(lep1) [GeV]",  100, 0., 300., "bjet_pt[0]",   "pT_b1",         "pT(bjet1) [GeV]",  100, 0., 300.,],
			     #["lep_pt[1]", "pT_l2", "pT(lep2) [GeV]",  100, 0., 300., "bjet_pt[1]",   "pT_b2",         "pT(bjet2) [GeV]",  100, 0., 300.,]		
			    # ["lep_pt[0]", "pT_l1", "pT(lep1) [GeV]",  100, 0., 300., "lep_pt[1]",    "pT_l2",         "pT(lep2) [GeV]",  100, 0., 300.,],			     	     
			     #["bjet_pt[0]", "pT_n1", "pT(bjet1) [GeV]",  100, 0., 300., "bjet_pt[1]",    "pT_b2",         "pT(bjet2) [GeV]",  100, 0., 300.,],			     	     			     

]

#observables2D = []


#Cuts optimised with NW considered and looking at the plots
cut_dphi_bbll = "abs(dPhibbll/TMath::Pi()) > 0.7"
cut_dR_bbll   = "dRbbll > 2.2"
cut_mll       = "mll < 70"
cut_mbb       = "mbb < 150 && mbb > 60"
cut_dR_ll     = "dRll < 1.6"
cut_dphi_bb   = "abs(dPhibb/TMath::Pi()) < 0.6"
cut_dR_bb     = "dRbb < 2" #this is quite sharp...
cut_dphi_ll   = "abs(dPhill/TMath::Pi()) < 0.4"
cut_pT_ll     = "pTll > 40"
cut_pT_bb     = "pTbb > 100"
cut_mbll      = "mbbll > 250"
cut_pT_b      = "bjet_pt > 60"


selections = [#["1",                            "e#mu, 2b",                     "2l_2b",                    UNBLIND],
 			  #["NW_weight >0 ",                "e#mu, 2b, NW > 0",             "2l_2b_NW_pass"], 			   			  
 			  ["NW_weight <0 ",                "e#mu, 2b, NW < 0",             "2l_2b_NW_fail"], 			  
 			  #["NW_weight <0 && dRbb <2",      "e#mu, 2b, NW < 0, dR(bb) < 2", "2l_2b_NW_fail_dRbb_less2", UNBLIND],
 			  #["NW_weight <0 && dRbb <2 && mll < 80 && dRll < 1.4 && pTbb > 120 && dRbbll > 2 && mbbll > 250 && abs(dPhill/TMath::Pi()) < 0.4 && abs(dPhibbll/TMath::Pi()) > 0.6 && abs(dPhill/TMath::Pi()) < 0.6",  "e#mu, 2b, NW < 0, SR", "2l_2b_SR", UNBLIND],
 			  #["NW_weight <0 && " + cut_dphi_bbll + " && " + cut_mll + " && " + cut_dR_bbll + " && mbb < 170 && dRll < TMath::Pi() & dRll < TMath::Pi() && abs(dPhibbll/TMath::Pi()) > 0.8 && dRbb > TMath::Pi() && abs(dPhill/TMath::Pi()) < 0.5",  "e#mu, 2b, NW < 0, SR", "2l_2b_SR", UNBLIND], 			  
 			  #["NW_weight <0 && " + cut_dphi_bbll + " && " + cut_mll + " && " + cut_dR_bbll + " && " + cut_mbb + " && " + cut_dR_ll + " && " + cut_dphi_bbll + " && " + cut_dR_bb + " && " + cut_dphi_ll + " && " + cut_pT_ll + " && " + cut_pT_bb + " && " + cut_mbll + " && " + cut_pT_b + " && " + cut_dphi_bb,  "e#mu, 2b, NW < 0, SR", "2l_2b_SR"], 			   			  
]


for selection in selections:
	for observable in observables1D:

		hist_bbWW  = TH1D("hist_bbWW"+ observable[1] + selection[2], ";"+observable[2], observable[3], observable[4], observable[5])
		hist_tt    = TH1D("hist_tt"  + observable[1] + selection[2], ";"+observable[2], observable[3], observable[4], observable[5])

		file_bbWW  = TFile(input_path + "DiHiggs_bbWW.root")
		gROOT.cd()
		file_tt    = TFile(input_path + "ttbar_dilep_skimmed.root")
		gROOT.cd()

		tree_bbWW  = file_bbWW.Get("AnalysisMiniTree")
		tree_tt    = file_tt.Get("AnalysisMiniTree")

		selection_cuts = selection[0]

		tree_bbWW.Project( hist_bbWW.GetName(),  observable[0], selection_cuts)
		tree_tt.Project(   hist_tt.GetName(),    observable[0], selection_cuts)

		hist_bbWW.Scale(1./hist_bbWW.Integral())
		hist_tt.Scale(1./hist_tt.Integral())	

		hist_tt.SetFillColor(0)
		hist_bbWW.SetFillColor(0)

		hist_tt.SetLineColor(COLOR_TTBAR)
		hist_bbWW.SetLineColor(COLOR_BBWW)

		hist_tt.SetMarkerColor(COLOR_TTBAR)
		hist_bbWW.SetMarkerColor(COLOR_BBWW)

		hist_tt.SetMarkerSize(0)
		hist_bbWW.SetMarkerSize(0)

		hist_tt.SetLineWidth(3)
		hist_bbWW.SetLineWidth(3)

		c1 = TCanvas()

		legend = TLegend(0.7,0.7,0.90,0.92)
		legend.AddEntry(hist_tt,   "t#bar{t} (dilep)", "L")
		legend.AddEntry(hist_bbWW, "HH (bbWW)",        "L")
		legend.SetLineColor(0)
		legend.SetFillColor(0)
		legend.SetTextSize(0.03)

		hist_tt.Draw("HIST")
		hist_bbWW.Draw("HIST SAME")

		if hist_bbWW.GetMaximum() > hist_tt.GetMaximum():
			hist_tt.SetMaximum(1.3*hist_bbWW.GetMaximum())

		legend.Draw("SAME")

		legend_header = selection[1]
		lumi_string = "#sqrt{s} = 13 TeV, 36 fb^{-1}"
		al.make_ATLAS_label( 0.2, 0.89,  1, c1, 0.035, "  Simulation Internal")
		al.make_ATLAS_string( 0.2, 0.84, lumi_string, 0.035)
		al.make_ATLAS_string( 0.2, 0.79, legend_header, 0.035)

		c1.Print(input_path + "MC_Cut_" + selection[2] + "_"+ observable[1] + ".pdf")


for selection in selections:
	for observable in observables2D:


		hist_bbWW  = TH2D("hist_bbWW"+ observable[1] + observable[7]+ selection[2], ";"+observable[2]+";"+observable[8], observable[3], observable[4], observable[5], observable[9], observable[10], observable[11])
		hist_tt    = TH2D("hist_tt"  + observable[1] + observable[7]+ selection[2], ";"+observable[2]+";"+observable[8], observable[3], observable[4], observable[5], observable[9], observable[10], observable[11])

		file_bbWW  = TFile(input_path + "DiHiggs_bbWW.root")
		gROOT.cd()
		file_tt    = TFile(input_path + "ttbar_dilep_skimmed.root")
		gROOT.cd()

		tree_bbWW  = file_bbWW.Get("AnalysisMiniTree")
		tree_tt    = file_tt.Get("AnalysisMiniTree")

		selection_cuts = selection[0]

		tree_bbWW.Project( hist_bbWW.GetName(),  observable[6]+":"+observable[0], selection_cuts)
		tree_tt.Project(   hist_tt.GetName(),    observable[6]+":"+observable[0], selection_cuts)

		hist_tt.SetFillColor(COLOR_TTBAR)
		hist_bbWW.SetFillColor(COLOR_BBWW)

		hist_tt.SetLineColor(COLOR_TTBAR)
		hist_bbWW.SetLineColor(COLOR_BBWW)

		hist_tt.SetMarkerColor(COLOR_TTBAR)
		hist_bbWW.SetMarkerColor(COLOR_BBWW)

		hist_tt.SetMarkerSize(0.2)
		hist_bbWW.SetMarkerSize(0.2)

		hist_tt.SetLineWidth(0)
		hist_bbWW.SetLineWidth(0)

		#legend = TLegend(0.7,0.7,0.90,0.92)
		#legend.AddEntry(hist_tt,   "t#bar{t} (dilep)", "F")
		#legend.AddEntry(hist_Wt,   "Wt (dilep)",       "F")
		#legend.AddEntry(hist_bbWW, "HH (bbWW)",        "F")
		#legend.AddEntry(hist_bbZZ, "HH (bbZZ)",        "F")
		#legend.AddEntry(hist_bbtt, "HH (bb#tau#tau)",  "F")
		#legend.SetLineColor(0)
		#legend.SetFillColor(0)
		#legend.SetTextSize(0.03)


		c1 = TCanvas()

		hist_tt.Draw("SCAT")
		hist_bbWW.Draw("SCAT SAME")

		#legend.Draw("SAME")

		#if observable[0] == "mbb":
	    #c1.SetLogy()

		#legend_header = selection[1]
		#lumi_string = "#sqrt{s} = 13 TeV, 36 fb^{-1}"
		#al.make_ATLAS_label( 0.2, 0.89,  1, c1, 0.035, "  Internal")
		#al.make_ATLAS_string( 0.2, 0.84, lumi_string, 0.035)
		#al.make_ATLAS_string( 0.2, 0.79, legend_header, 0.035)


		c1.Print(input_path + "MC_Cut_" + selection[2] + "_"+ observable[1] + "_" + observable[7] + ".pdf")
